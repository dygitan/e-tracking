package rpo.utility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class MessageUtil {

    public static String RECORD_ADDED = "Record has been successfully added.";
    public static String RECORD_UPDATED = "Record has been successfully updated.";
    public static String RECORD_DELETED = "Record has been successfully deleted.";
}
