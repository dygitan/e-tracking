package rpo.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import rpo.constant.AppConstant;

/**
 *
 * @author Patrick Tan
 */
public class AppUtility {

    private static final SimpleDateFormat sdf = new SimpleDateFormat(AppConstant.DEFAULT_DATE_TIME_FORMAT);

    public static Date toDate(String str) {
        try {
            return sdf.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(AppUtility.class).error(ex);
            return null;
        }
    }

    public static String calculateDiff(Date startDatetime, Date endDatetime) {
        long different = endDatetime.getTime() - startDatetime.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        StringBuilder sb = new StringBuilder();

        if (elapsedDays != 0) {
            sb.append(elapsedDays);
            sb.append(" days ");
        }

        if (elapsedHours != 0) {
            sb.append(elapsedHours);
            sb.append(" hours ");
        }

        if (elapsedMinutes != 0) {
            sb.append(elapsedMinutes);
            sb.append(" mins ");
        }

        if (elapsedSeconds != 0) {
            sb.append(elapsedSeconds);
            sb.append(" seconds");
        }

        return sb.toString();
    }
}
