package rpo.config.spring;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;
import rpo.config.security.SecurityConfig;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@EnableWebMvc
@Configuration
@ComponentScan({"rpo.*"})
@EnableTransactionManagement
@EnableAspectJAutoProxy
@Import({SecurityConfig.class})
@PropertySource("classpath:application.properties")
public class SpringConfig {

    private static final Logger LOGGER = Logger.getLogger(SpringConfig.class);
    @Autowired
    private Environment env;

    @Bean(name = "sessionFactory")
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
        builder.scanPackages("rpo.model").addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }

    private Properties getHibernateProperties() {
        Properties prop = new Properties();

        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

        return prop;
    }

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClass("com.mysql.jdbc.Driver");

        ds.setJdbcUrl(env.getProperty("database.url"));
        ds.setUser(env.getProperty("database.username"));
        ds.setPassword(env.getProperty("database.password"));

//        ds.setJdbcUrl("jdbc:mysql://127.6.211.2:3306/e_tracking_db");
//        ds.setUser("admink6SVGuq");
//        ds.setPassword("B1UGmzfrX5U_");
        return ds;
    }

    @Bean
    public HibernateTransactionManager txManager() {
        return new HibernateTransactionManager(sessionFactory());
    }

//    @Bean
//    public InternalResourceViewResolver viewResolver() {
//        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setViewClass(JstlView.class);
//        viewResolver.setPrefix("/WEB-INF/pages/");
//        viewResolver.setSuffix(".jsp");
//        return viewResolver;
//    }
    @Bean
    public TilesViewResolver viewResolver() {
        TilesViewResolver resolver = new TilesViewResolver();
        return resolver;
    }

    @Bean
    public TilesConfigurer tileConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();

//        tilesConfigurer.setDefinitions("/WEB-INF/tiles/general.xml");
        tilesConfigurer.setDefinitions("/WEB-INF/tiles/*.xml");
//        tilesConfigurer.setDefinitions(definitions.toArray(new String[definitions.size()]));

        return tilesConfigurer;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        return multipartResolver;
    }

    @Bean
    public JavaMailSenderImpl javaMail() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setUsername(env.getProperty("mail.username"));
        mailSender.setPassword(env.getProperty("mail.password"));

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.debug", "true");
//        props.put("mail.debug.auth", "true");
        props.put("mail.smtp.host", env.getProperty("mail.host"));
        props.put("mail.smtp.port", env.getProperty("mail.port"));

        mailSender.setJavaMailProperties(props);

        return mailSender;
    }
}
