package rpo.config.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.header.HeaderWriter;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = Logger.getLogger(SecurityConfig.class);
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring()
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        List<String> s = new ArrayLis;

        http.headers().frameOptions().addHeaderWriter(new HeaderWriter() {

            @Override
            public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {
                response.setHeader("X-Frame-Options", "SAMEORIGIN");
            }
        });

//        http.headers()
//                .addHeaderWriter(new XFrameOptionsHeaderWriter(new WhiteListedAllowFromStrategy(null)));
        http.authorizeRequests()
                .antMatchers("/secured/dashboard").hasAnyRole("ADMIN", "EMPLOYEE", "SUPER_ADMIN")
                .antMatchers("/secured/employee/log").hasAnyRole("ADMIN", "EMPLOYEE", "SUPER_ADMIN")
                .antMatchers("/secured/employee/add").hasAnyRole("ADMIN", "SUPER_ADMIN")
                .antMatchers("/secured/employee/json/all").hasAnyRole("ADMIN", "SUPER_ADMIN")
                .antMatchers("/secured/employee/view/all").hasAnyRole("ADMIN", "SUPER_ADMIN")
                .antMatchers("/secured/employee/delete/**").hasAnyRole("SUPER_ADMIN")                
                .antMatchers("/secured/user/**").hasAnyRole("SUPER_ADMIN")
                .antMatchers("/public/**").anonymous()
                .and()
                .formLogin()
                .loginPage("/public/login")
                .failureUrl("/public/login?error")
                .defaultSuccessUrl("/secured/dashboard")
                .permitAll();

//                .and()
        // Example Remember Me Configuration
//                .rememberMe();
    }

    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}
