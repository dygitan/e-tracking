package rpo.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
