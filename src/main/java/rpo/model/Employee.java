package rpo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import rpo.constant.AppConstant;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Entity
@Table(name = "employee", uniqueConstraints = @UniqueConstraint(columnNames = "employee_username"))
@AttributeOverride(name = "primaryId", column = @Column(name = "employee_id"))
public class Employee extends BaseModel {

    @NotBlank
    @Size(max = 45)
    private String lastName;
    @NotBlank
    @Size(max = 45)
    private String firstName;
    @NotBlank
    @Size(max = 45)
    private String contactNo;
    @NotBlank
    @Size(max = 200)
    private String address;
    @NotBlank
    @Size(max = 45)
    private String securityLicenseNo;
    @JsonFormat(pattern = AppConstant.DEFAULT_DATE_FORMAT, shape = JsonFormat.Shape.STRING)
    @NotNull
    private Date securityLicenseExpiry;
    private byte[] securityLicenseFile;
    private String securityLicenseFileType;
    @NotBlank
    @Size(max = 45)
    private String firstAidLicenseNo;
    @JsonFormat(pattern = AppConstant.DEFAULT_DATE_FORMAT, shape = JsonFormat.Shape.STRING)
    @NotNull
    private Date firstAidLicenseExpiry;
    private byte[] firstAidLicenseFile;
    private String firstAidLicenseFileType;
    @Valid
    @NotNull
    private SecUser employeeUsername;
    private Set<EmployeeLog> employeeLogs = new HashSet<>(0);
    private Set<EmployeeAvailability> employeeAvailabilities = new HashSet<>(0);

    public Employee() {
    }

    public Employee(Integer primaryId) {
        setPrimaryId(primaryId);
    }

    @Column(name = "last_name", nullable = false, length = 45)
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "first_name", nullable = false, length = 45)
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "contact_no", nullable = false, length = 45)
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Column(name = "address", nullable = false, length = 200)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "security_license_no", nullable = false, length = 45)
    public String getSecurityLicenseNo() {
        return securityLicenseNo;
    }

    public void setSecurityLicenseNo(String securityLicenseNo) {
        this.securityLicenseNo = securityLicenseNo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "security_license_expiry")
    public Date getSecurityLicenseExpiry() {
        return securityLicenseExpiry;
    }

    public void setSecurityLicenseExpiry(Date securityLicenseExpiry) {
        this.securityLicenseExpiry = securityLicenseExpiry;
    }

    @Column(name = "security_license_file")
    @JsonIgnore
    public byte[] getSecurityLicenseFile() {
        return securityLicenseFile;
    }

    public void setSecurityLicenseFile(byte[] securityLicenseFile) {
        this.securityLicenseFile = securityLicenseFile;
    }

    @Column(name = "security_license_file_type")
    public String getSecurityLicenseFileType() {
        return securityLicenseFileType;
    }

    public void setSecurityLicenseFileType(String securityLicenseFileType) {
        this.securityLicenseFileType = securityLicenseFileType;
    }

    @Column(name = "first_aid_license_no", nullable = false, length = 45)
    public String getFirstAidLicenseNo() {
        return firstAidLicenseNo;
    }

    public void setFirstAidLicenseNo(String firstAidLicenseNo) {
        this.firstAidLicenseNo = firstAidLicenseNo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "first_aid_license_expiry")
    public Date getFirstAidLicenseExpiry() {
        return firstAidLicenseExpiry;
    }

    public void setFirstAidLicenseExpiry(Date firstAidLicenseExpiry) {
        this.firstAidLicenseExpiry = firstAidLicenseExpiry;
    }

    @Column(name = "first_aid_license_file")
    @JsonIgnore
    public byte[] getFirstAidLicenseFile() {
        return firstAidLicenseFile;
    }

    public void setFirstAidLicenseFile(byte[] firstAidLicenseFile) {
        this.firstAidLicenseFile = firstAidLicenseFile;
    }

    @Column(name = "first_aid_license_file_type")
    public String getFirstAidLicenseFileType() {
        return firstAidLicenseFileType;
    }

    public void setFirstAidLicenseFileType(String firstAidLicenseFileType) {
        this.firstAidLicenseFileType = firstAidLicenseFileType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_username", unique = true)
    public SecUser getEmployeeUsername() {
        return employeeUsername;
    }

    public void setEmployeeUsername(SecUser employeeUsername) {
        this.employeeUsername = employeeUsername;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    public Set<EmployeeLog> getEmployeeLogs() {
        return employeeLogs;
    }

    public void setEmployeeLogs(Set<EmployeeLog> employeeLogs) {
        this.employeeLogs = employeeLogs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @OrderBy("freeDay")
    public Set<EmployeeAvailability> getEmployeeAvailabilities() {
        return this.employeeAvailabilities;
    }

    public void setEmployeeAvailabilities(Set<EmployeeAvailability> employeeAvailabilities) {
        this.employeeAvailabilities = employeeAvailabilities;
    }

//    @Transient
//    public List<EmployeeAvailability> getEmployeeAvailabilitiesList() {
//        return new ArrayList<>(getEmployeeAvailabilities());
//    }
}
