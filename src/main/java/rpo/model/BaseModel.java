package rpo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.commons.lang3.StringUtils;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@MappedSuperclass
public class BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer primaryId;
    private String encryptedKey;
    private SecUser createdBy;
    private Date createdDate;
    private SecUser updatedBy;
    private Date updatedDate;
    private Boolean deleted;

    /**
     * @return the primaryId
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    @JsonIgnore
    public Integer getPrimaryId() {
        if (!StringUtils.isBlank(encryptedKey)) {
            primaryId = SecurityUtility.decryptId(encryptedKey);
        }

        return primaryId;
    }

    /**
     * @param primaryId the primaryId to set
     */
    public void setPrimaryId(Integer primaryId) {
        this.primaryId = primaryId;
    }

    /**
     * @return the encryptedKey
     */
    @Transient
    @JsonProperty(value = "encryptedKey", required = true)
    public String getEncryptedKey() {
        if (StringUtils.isBlank(encryptedKey) && primaryId != null) {
            encryptedKey = SecurityUtility.encryptKey(primaryId);
        }

        return encryptedKey;
    }

    /**
     * @param encryptedKey the encryptedKey to set
     */
    public void setEncryptedKey(String encryptedKey) {
        this.encryptedKey = encryptedKey;
    }

    /**
     * @return the createdBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", nullable = false)
    public SecUser getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(SecUser createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false, length = 19)
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the updatedBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updated_by")
    public SecUser getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(SecUser updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the updatedDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date", length = 19)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "deleted")
    public Boolean getDeleted() {
        if (deleted == null) {
            return false;
        }
        
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
