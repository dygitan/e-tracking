package rpo.model;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import rpo.enums.DayEnum;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Entity
@Table(name = "employee_availability")
@AttributeOverride(name = "primaryId", column = @Column(name = "availability_id"))
public class EmployeeAvailability extends BaseModel {

    private Employee employee;
    private int freeDay;
    private boolean available;
    private Date freeTime;
    private Date freeTimeEnd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Column(name = "free_day", nullable = false)
    public int getFreeDay() {
        return this.freeDay;
    }

    public void setFreeDay(int freeDay) {
        this.freeDay = freeDay;
    }

    @Column(name = "available")
    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "free_time", length = 8)
    public Date getFreeTime() {
        return this.freeTime;
    }

    public void setFreeTime(Date freeTime) {
        this.freeTime = freeTime;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "free_time_end", length = 8)
    public Date getFreeTimeEnd() {
        return freeTimeEnd;
    }

    public void setFreeTimeEnd(Date freeTimeEnd) {
        this.freeTimeEnd = freeTimeEnd;
    }

    @Transient
    public String getStrDay() {
        return DayEnum.getLabel(freeDay);
    }

}
