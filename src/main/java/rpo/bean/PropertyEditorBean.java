package rpo.bean;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import rpo.constant.AppConstant;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class PropertyEditorBean extends PropertyEditorSupport {

    private static final ObjectMapper mapper = new ObjectMapper();
    private final Class<?> clazz;

    public PropertyEditorBean(Class<?> clazz) {
        mapper.setDateFormat(new SimpleDateFormat(AppConstant.DEFAULT_DATE_TIME_FORMAT));
        this.clazz = clazz;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            setValue(mapper.readValue(text, clazz));
        } catch (IOException ex) {
            Logger.getLogger(PropertyEditorBean.class).error(ex);
        }
    }
}
