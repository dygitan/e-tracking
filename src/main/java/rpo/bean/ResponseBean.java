package rpo.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.validation.FieldError;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@JsonIgnoreProperties({"errors.bindingFailure"})
public class ResponseBean {

    private String message;
    private Map data = new HashMap();
    private List<FieldError> errors;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the data
     */
    public Map getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Map data) {
        this.data = data;
    }

    /**
     * @return the errors
     */
    public List<FieldError> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<FieldError> errors) {
        this.errors = errors;
    }
}
