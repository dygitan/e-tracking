package rpo.bean;

import javax.persistence.Transient;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class PasswordBean {

    private String email;
    @NotEmpty
    private String currentPassword;
    @NotEmpty
    @Size(min = 6)
    private String newPassword;
    private String confirmPassword;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @AssertTrue(message = "the password entered did not matched.")
    @Transient
    public boolean isValid() {
        if (StringUtils.isEmpty(currentPassword)) {
            return !StringUtils.isEmpty(newPassword) && newPassword.equals(confirmPassword);
        } else if (StringUtils.isEmpty(newPassword) && StringUtils.isEmpty(confirmPassword)) {
            return true;
        } else {
            return !StringUtils.isEmpty(newPassword) && newPassword.equals(confirmPassword);
        }
    }
}
