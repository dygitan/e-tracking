package rpo.bean;

import javax.validation.Valid;
import org.springframework.web.multipart.MultipartFile;
import rpo.model.Employee;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class EmployeeBean {

    @Valid
    private Employee employee;
    private MultipartFile securityLicenseUpload;
    private MultipartFile firstAidLicenseUpload;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public MultipartFile getSecurityLicenseUpload() {
        return securityLicenseUpload;
    }

    public void setSecurityLicenseUpload(MultipartFile securityLicenseUpload) {
        this.securityLicenseUpload = securityLicenseUpload;
    }

    public MultipartFile getFirstAidLicenseUpload() {
        return firstAidLicenseUpload;
    }

    public void setFirstAidLicenseUpload(MultipartFile firstAidLicenseUpload) {
        this.firstAidLicenseUpload = firstAidLicenseUpload;
    }
}
