package rpo.bean;

import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Patrick Tan
 */
public class UserBean {

    @NotEmpty
    private String username;
    @NotEmpty
    @Email
    private String email;
    private String role;
    @NotNull
    private Integer roleId;
    private boolean enabled;
    private String password;
    private String enteredPassword;
    private String confirmPassword;

    public UserBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEnteredPassword() {
        return enteredPassword;
    }

    public void setEnteredPassword(String enteredPassword) {
        this.enteredPassword = enteredPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @AssertTrue(message = "the password entered did not matched.")
    @Valid
    public boolean isPasswordValid() {
        if (StringUtils.isEmpty(password)) {
            return !StringUtils.isEmpty(enteredPassword) && enteredPassword.equals(confirmPassword);
        } else if (StringUtils.isEmpty(enteredPassword) && StringUtils.isEmpty(confirmPassword)) {
            return true;
        } else {
            return !StringUtils.isEmpty(enteredPassword) && enteredPassword.equals(confirmPassword);
        }
    }

    @AssertTrue(message = "adding of new employee is not allowed on this module.")
    @Valid
    public boolean isNotEmployeeUser() {
        if (!StringUtils.isEmpty(password)) {
            return true;
        } else if (roleId == null) {
            return true;
        } else {
            return roleId != 2;
        }
    }
}
