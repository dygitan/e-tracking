package rpo.aop;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import rpo.model.BaseModel;
import rpo.model.SecUser;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Configuration
@Aspect
public class LogginAspect {

    private static final Logger LOGGER = Logger.getLogger(LogginAspect.class);

    @Before("execution(* rpo.dao.impl.*.saveOrUpdate(..))")
//    @Before("@annotation(rpo.annotations.Auditable)")
    public void logBefore(JoinPoint joinPoint) {

        Object temp = joinPoint.getArgs()[0];

        LOGGER.debug("Triggered saveOrUpdate method (" + temp + ")...");

        if (temp instanceof BaseModel) {
            BaseModel base = (BaseModel) temp;

            LOGGER.debug("base: " + base);

            if (base.getDeleted() == null) {
                base.setDeleted(Boolean.FALSE);
            }

            if (StringUtils.isEmpty(base.getEncryptedKey())) {
                if (base.getCreatedBy() == null || StringUtils.isEmpty(base.getCreatedBy().getUsername())) {
                    base.setCreatedBy(new SecUser(SecurityUtility.getCurrentUsername()));
                }

                base.setCreatedDate(new Date());
            } else {
                base.setUpdatedBy(new SecUser(SecurityUtility.getCurrentUsername()));
                base.setUpdatedDate(new Date());
            }
        } else {
            LOGGER.warn("Args [" + temp + "] is not an instance of BaseModel.");
        }
    }
}
