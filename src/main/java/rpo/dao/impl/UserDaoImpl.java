package rpo.dao.impl;

import org.hibernate.FetchMode;
import org.springframework.stereotype.Repository;
import rpo.dao.UserDao;
import rpo.model.SecUser;
import rpo.query.QueryBean;
import rpo.query.QueryCondition;
import rpo.query.QueryCriteria;

/**
 *
 * @author Patrick Tan
 * @since Dec 6, 2014
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<SecUser> implements UserDao {

    public UserDaoImpl() {
        super(SecUser.class);
    }

    @Override
    public SecUser getUserDetails(String username) {
        QueryBean queryBean = new QueryBean(SecUser.class);

        queryBean.addCriteria(new QueryCriteria("secUserRoles.lookupUserRole", "lookupUserRole"));
        queryBean.addCriteria(new QueryCriteria("secUserRoles", FetchMode.JOIN));

        queryBean.addCondition(new QueryCondition("username", username));

        return get(queryBean);
    }
}
