package rpo.dao.impl;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import rpo.dao.GenericDao;
import rpo.query.QueryBean;
import rpo.query.QueryBuilder;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 * @param <T>
 */
@Repository(value = "genericDao")
public class GenericDaoImpl<T> implements GenericDao<T> {

    @Autowired
    private QueryBuilder queryBuilder;
    @Autowired
    private SessionFactory sessionFactory;
    private Class clz;

    public GenericDaoImpl() {
    }

    public GenericDaoImpl(Class T) {
        this.clz = T;
    }

    @Override
    public void delete(T t) {
        getSession().delete(t);
    }

    @Override
    public T get(String idName, Integer id) {
        Criteria criteria = getSession().createCriteria(clz);
        criteria.add(Restrictions.eq(idName, id));
        return (T) criteria.uniqueResult();
    }

    @Override
    public T get(QueryBean queryBean) {
        return (T) queryBuilder.buildQuery(queryBean).uniqueResult();
    }

    @Override
    public List<T> getListByGeneric(QueryBean queryBean) {
        return queryBuilder.buildQuery(queryBean).list();
    }

    @Override
    public void saveOrUpdate(T t) {
        getSession().saveOrUpdate(t);
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
