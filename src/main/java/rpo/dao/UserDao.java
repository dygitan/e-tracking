package rpo.dao;

import rpo.model.SecUser;

/**
 *
 * @author Patrick Tan
 * @since Dec 6, 2014
 */
public interface UserDao {

    SecUser getUserDetails(String username);
}
