package rpo.dao;

import java.util.List;
import rpo.query.QueryBean;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 * @param <T>
 */
public interface GenericDao<T> {

    void delete(T entity);

    T get(String idName, Integer id);

    T get(QueryBean queryBean);

    List<T> getListByGeneric(QueryBean queryBean);

    void saveOrUpdate(T t);
}
