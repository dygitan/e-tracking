/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpo.json.transformer;

import flexjson.ObjectBinder;
import flexjson.transformer.DateTransformer;
import java.lang.reflect.Type;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Patrick Tan
 */
public class CustomDateTransformer extends DateTransformer {

    public CustomDateTransformer(String dateFormat) {
        super(dateFormat);
    }

    @Override
    public Object instantiate(ObjectBinder context, Object value, Type targetType, Class targetClass) {
        if (value == null || StringUtils.isBlank(value.toString())) {
            return null;
        }

        return super.instantiate(context, value, targetType, targetClass);
    }

}
