package rpo.json.transformer;

/**
 *
 * @author Patrick Tan
 */
public class BooleanTransformer extends flexjson.transformer.BooleanTransformer {

    public BooleanTransformer() {
    }

    @Override
    public void transform(Object object) {
        Boolean o;

        if (object == null) {
            o = Boolean.FALSE;
        } else {
            o = Boolean.valueOf(object.toString());
        }

        super.transform(o);

    }

}
