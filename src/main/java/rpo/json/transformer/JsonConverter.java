package rpo.json.transformer;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Patrick Tan
 */
public class JsonConverter {

    public static final CustomDateTransformer DATE_TRANSFORMER = new CustomDateTransformer("MM/dd/yyyy");
    public static final CustomDateTransformer DATE_TIME_TRANSFORMER = new CustomDateTransformer("MM/dd/yyyy - hh:mm a");
    private static final BooleanTransformer X = new BooleanTransformer();

    public static <T> T deserialize(Class<T> cls, String json) {
        return deserialize(cls, json, DATE_TRANSFORMER);
    }

    public static <T> T deserialize(Class<T> cls, String json, DateTransformer dateTransformer) {
        return deserialize(cls, json, null, dateTransformer);
    }

    public static <T> T deserialize(Class<T> cls, String json, Map<String, Class> map, DateTransformer dateTransformer) {
        JSONDeserializer<T> deserializer = new JSONDeserializer<>();
        deserializer.use(Date.class, dateTransformer);

        if (map != null) {
            Iterator<String> iterator = map.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                deserializer.use(key, map.get(key));
            }
        }

        return deserializer.deserialize(json, cls);
    }

    public static String serialize(Object o) {
        return serialize(o, null, null);
    }

    public static String serialize(Object o, List<String> included, List<String> excluded) {
        JSONSerializer serializer = new JSONSerializer();

        if (included != null && !included.isEmpty()) {
            serializer.include(included.toArray(new String[]{}));
        }

        if (excluded != null && !excluded.isEmpty()) {
            excluded.add("*.class");
            serializer.exclude(excluded.toArray(new String[]{}));
        } else {
            serializer.exclude("*.class");
        }

        serializer.transform(X, Boolean.class);

        return serializer.serialize(o);
    }
}
