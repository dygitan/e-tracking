package rpo.constant;

/**
 *
 * @author Patrick Tan
 * @since Dec 4, 2014
 */
public interface AppConstant {

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
}
