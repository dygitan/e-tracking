package rpo.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public class QueryOrdering {

    private String field;
    private QueryOrderType queryOrderType;

    public QueryOrdering() {
    }

    public QueryOrdering(String field, QueryOrderType queryOrderType) {
        this.field = field;
        this.queryOrderType = queryOrderType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public QueryOrderType getQueryOrderType() {
        return queryOrderType;
    }

    public void setQueryOrderType(QueryOrderType queryOrderType) {
        this.queryOrderType = queryOrderType;
    }
}
