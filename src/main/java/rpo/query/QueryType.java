package rpo.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public enum QueryType {

    FETCH, ALIAS;

    private QueryType() {
    }

}
