package rpo.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public class QueryCondition {

    private String field;
    private Object value;
    private QueryConditionType conditionType;

    public QueryCondition() {
    }

    public QueryCondition(String field, Object value) {
        this.field = field;
        this.value = value;
        this.conditionType = QueryConditionType.EQUAL;
    }

    public QueryCondition(String field, Object value, QueryConditionType conditionType) {
        this.field = field;
        this.value = value;
        this.conditionType = conditionType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public QueryConditionType getConditionType() {
        return conditionType;
    }

    public void setConditionType(QueryConditionType conditionType) {
        this.conditionType = conditionType;
    }
}
