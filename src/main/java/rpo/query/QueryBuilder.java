package rpo.query;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static rpo.query.QueryConditionType.LE;
import static rpo.query.QueryConditionType.NOT_EQUAL;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
@Component
public class QueryBuilder {

    private static final Logger LOGGER = Logger.getLogger(QueryBuilder.class);
    @Autowired
    private SessionFactory sessionFactory;

    public Criteria buildQuery(QueryBean queryBean) {
        Criteria criteria = getSession().createCriteria(queryBean.getCls());

        if (queryBean.getModel() != null) {
            for (QueryCriteria queryCriteria : queryBean.getModel()) {
                if (queryCriteria.getQueryType() == QueryType.ALIAS) {
                    if (queryCriteria.getCriterion() != null) {
                        criteria.createAlias(queryCriteria.getReferenceName(),
                                queryCriteria.getAliasName(),
                                queryCriteria.getJoinType(),
                                queryCriteria.getCriterion());
                    } else {
                        criteria.createAlias(queryCriteria.getReferenceName(),
                                queryCriteria.getAliasName(),
                                queryCriteria.getJoinType());
                    }
                } else if (queryCriteria.getQueryType() == QueryType.FETCH) {
                    criteria.setFetchMode(queryCriteria.getReferenceName(),
                            queryCriteria.getFetchMode());
                }
            }
        }

        if (queryBean.getConditions() != null) {
            for (QueryCondition condition : queryBean.getConditions()) {

                switch (condition.getConditionType()) {
                    case GE:
                        criteria.add(Restrictions.ge(condition.getField(), condition.getValue()));
                        break;
                    case NOT_EQUAL:
                        criteria.add(Restrictions.ne(condition.getField(), condition.getValue()));
                        break;
                    case LE:
                        criteria.add(Restrictions.le(condition.getField(), condition.getValue()));
                        break;
                    case SQL:
                        criteria.add(Restrictions.sqlRestriction(condition.getValue().toString()));
                        break;
                    default:
                        criteria.add(Restrictions.eq(condition.getField(), condition.getValue()));
                        break;
                }
            }
        }

        if (queryBean.getOrdering() != null) {
            for (QueryOrdering ordering : queryBean.getOrdering()) {
                if (ordering.getQueryOrderType() == QueryOrderType.ASC) {
                    criteria.addOrder(Order.asc(ordering.getField()));
                } else {
                    criteria.addOrder(Order.desc(ordering.getField()));
                }
            }
        }

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return criteria;
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
