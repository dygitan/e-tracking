package rpo.query;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Criterion;
import org.hibernate.sql.JoinType;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public class QueryCriteria {

    private String referenceName;
    private String aliasName;
    private FetchMode fetchMode;
    private JoinType joinType;
    private QueryType queryType;
    private Criterion criterion;

    public QueryCriteria() {
    }

    public QueryCriteria(String referenceName) {
        this.referenceName = referenceName;
        this.aliasName = referenceName;
        this.queryType = QueryType.ALIAS;
        this.joinType = JoinType.LEFT_OUTER_JOIN;
    }

    public QueryCriteria(String referenceName, String aliasName) {
        this.referenceName = referenceName;
        this.aliasName = aliasName;
        this.queryType = QueryType.ALIAS;
        this.joinType = JoinType.LEFT_OUTER_JOIN;
    }

    public QueryCriteria(String referenceName, String aliasName, JoinType joinType) {
        this.referenceName = referenceName;
        this.aliasName = aliasName;
        this.queryType = QueryType.ALIAS;
        this.joinType = joinType;
    }

    public QueryCriteria(String referenceName, String aliasName, Criterion criterion) {
        this.referenceName = referenceName;
        this.aliasName = aliasName;
        this.queryType = QueryType.ALIAS;
        this.criterion = criterion;
    }

    public QueryCriteria(String referenceName, FetchMode fetchMode) {
        this.referenceName = referenceName;
        this.fetchMode = fetchMode;
        this.queryType = QueryType.FETCH;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public FetchMode getFetchMode() {
        return fetchMode;
    }

    public void setFetchMode(FetchMode fetchMode) {
        this.fetchMode = fetchMode;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinType joinType) {
        this.joinType = joinType;
    }

    public Criterion getCriterion() {
        return criterion;
    }

    public void setCriterion(Criterion criterion) {
        this.criterion = criterion;
    }
}
