package rpo.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public enum QueryConditionType {

    EQUAL, GE, NOT_EQUAL, LE, SQL;

    private QueryConditionType() {
    }

}
