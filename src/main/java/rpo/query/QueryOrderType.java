package rpo.query;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public enum QueryOrderType {

    ASC, DESC;

    private QueryOrderType() {
    }

}
