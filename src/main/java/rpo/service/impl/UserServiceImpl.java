package rpo.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.FetchMode;
import org.hibernate.jpa.criteria.expression.function.CurrentDateFunction;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rpo.bean.PasswordBean;
import rpo.bean.UserBean;
import rpo.bean.UserDetailsBean;
import rpo.dao.GenericDao;
import rpo.dao.UserDao;
import rpo.model.SecUser;
import rpo.model.SecUserRole;
import rpo.model.lookup.LookupUserRole;
import rpo.query.QueryBean;
import rpo.query.QueryCondition;
import rpo.query.QueryCriteria;
import rpo.service.UserService;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan
 * @since Dec 6, 2014
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    @Autowired
    private UserDao userDao;
    @Autowired
    private GenericDao genericDao;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("public UserDetailsBean loadUserByUsername(String username) ... ");

        try {
            LOGGER.debug("enter username ... " + username);

            rpo.model.SecUser user = userDao.getUserDetails(username);

            LOGGER.debug("User >> " + user + " >>> " + user.getSecUserRoles().size());

            List<GrantedAuthority> authorities = buildUserAuthority(user.getSecUserRoles());

            return buildUserForAuthentication(user, authorities);
        } catch (Exception e) {
            LOGGER.debug("Oops! User with username [" + username + "] was not found.", e);

            throw new UsernameNotFoundException("ATSException: Username not found", e);
        }
    }

    private UserDetailsBean buildUserForAuthentication(rpo.model.SecUser user,
            List<GrantedAuthority> authorities) {

        UserDetailsBean bean = new UserDetailsBean(user.getUsername(), user.getPassword(),
                user.getEnabled(), true, true, true, authorities);

        bean.setSecUser(user);

        return bean;
    }

    private List<GrantedAuthority> buildUserAuthority(Set<SecUserRole> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<>();

        for (SecUserRole userRole : userRoles) {
            LOGGER.debug("Role [" + userRole.getLookupUserRole().getDescription() + "]");
            setAuths.add(new SimpleGrantedAuthority(userRole.getLookupUserRole().getDescription()));
        }

        List<GrantedAuthority> Result = new ArrayList<>(setAuths);

        return Result;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void changePassword(PasswordBean passwordBean) {
        SecUser secUser = SecurityUtility.getCurrentUser().getSecUser();

        secUser.setEmail(passwordBean.getEmail());
        secUser.setPassword(SecurityUtility.encryptPassword(passwordBean.getNewPassword()));
        secUser.setConfirmPassword(passwordBean.getConfirmPassword());
        secUser.setEnteredPassword(passwordBean.getConfirmPassword());

        genericDao.saveOrUpdate(secUser);
    }

    @Transactional(readOnly = true)
    @Override
    public SecUser findByUsername(String username) {
        return userDao.getUserDetails(username);
    }

    @Transactional(readOnly = true)
    @Override
    public List<UserBean> retrieveAll() {
        QueryBean queryBean = new QueryBean(SecUser.class);

        queryBean.addCriteria(new QueryCriteria("secUserRoles", FetchMode.JOIN));
        queryBean.addCriteria(new QueryCriteria("secUserRoles.lookupUserRole", "lookupUserRole"));

        List<UserBean> users = new ArrayList<>();
        List<SecUser> secUsers = genericDao.getListByGeneric(queryBean);

        for (SecUser user : secUsers) {
            users.add(convert(user));
        }

        return users;
    }

    @Transactional(readOnly = true)
    @Override
    public UserBean getUserBeanByUsername(String username) {
        return convert(userDao.getUserDetails(username));
    }

    private UserBean convert(SecUser user) {
        LookupUserRole userRole = new ArrayList<>(user.getSecUserRoles()).get(0).getLookupUserRole();
        String role = userRole.getDescription();

        if ("ROLE_EMPLOYEE".equalsIgnoreCase(role)) {
            role = "Employee";
        } else if ("ROLE_ADMIN".equals(role)) {
            role = "Admin";
        } else {
            role = "Super Admin";
        }

        UserBean userBean = new UserBean();

        userBean.setUsername(user.getUsername());
        userBean.setEmail(user.getEmail());
        userBean.setRole(role);
        userBean.setRoleId(userRole.getRoleId());
        userBean.setEnabled(user.getEnabled());
        userBean.setPassword(user.getPassword());

        return userBean;
    }

    @Transactional(readOnly = true)
    @Override
    public List<UserBean> retrieveAllSuperAdmin() {
        QueryBean queryBean = new QueryBean(SecUser.class);

        queryBean.addCriteria(new QueryCriteria("secUserRoles", FetchMode.JOIN));
        queryBean.addCriteria(new QueryCriteria("secUserRoles.lookupUserRole", "lookupUserRole"));

        queryBean.addCondition(new QueryCondition("lookupUserRole.description", "ROLE_SUPER_ADMIN"));

        List<UserBean> users = new ArrayList<>();
        List<SecUser> secUsers = genericDao.getListByGeneric(queryBean);

        for (SecUser user : secUsers) {
            users.add(convert(user));
        }

        return users;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdate(UserBean user) {
        SecUser currentSecUser = findByUsername(user.getUsername());

        if (currentSecUser != null) {
            currentSecUser.setUsername(user.getUsername());

            if (!StringUtils.isEmpty(user.getEnteredPassword())) {
                currentSecUser.setEnteredPassword(user.getEnteredPassword());
                currentSecUser.setConfirmPassword(user.getPassword());

                currentSecUser.setPassword(SecurityUtility.encryptPassword(user.getEnteredPassword()));
            } else {
                currentSecUser.setEnteredPassword(currentSecUser.getPassword());
                currentSecUser.setConfirmPassword(currentSecUser.getPassword());
            }

            currentSecUser.setEmail(user.getEmail());
            currentSecUser.setEnabled(user.isEnabled());

            genericDao.saveOrUpdate(currentSecUser);

            SecUserRole secUserRole = new ArrayList<>(currentSecUser.getSecUserRoles()).get(0);

            secUserRole.setLookupUserRole(new LookupUserRole(user.getRoleId()));
            secUserRole.setSecUser(currentSecUser);

            genericDao.saveOrUpdate(secUserRole);
        } else {
            SecUser secUser = new SecUser();

            secUser.setUsername(user.getUsername());
            secUser.setPassword(SecurityUtility.encryptPassword(user.getEnteredPassword()));
            secUser.setEnteredPassword(user.getEnteredPassword());
            secUser.setConfirmPassword(user.getEnteredPassword());
            secUser.setEmail(user.getEmail());
            secUser.setEnabled(user.isEnabled());

            genericDao.saveOrUpdate(secUser);

            SecUserRole secUserRole = new SecUserRole(new LookupUserRole(user.getRoleId()),
                    secUser);

            genericDao.saveOrUpdate(secUserRole);
        }
    }
}
