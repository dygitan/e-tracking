package rpo.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.FetchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rpo.annotations.Auditable;
import rpo.bean.SearchBean;
import rpo.bean.UserBean;
import rpo.dao.GenericDao;
import rpo.exception.CustomException;
import rpo.model.Employee;
import rpo.model.EmployeeAvailability;
import rpo.model.SecUser;
import rpo.model.SecUserRole;
import rpo.model.lookup.LookupUserRole;
import rpo.query.QueryBean;
import rpo.query.QueryCondition;
import rpo.query.QueryCriteria;
import rpo.query.QueryOrderType;
import rpo.query.QueryOrdering;
import rpo.service.EmployeeService;
import rpo.service.UserService;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOGGER = Logger.getLogger(EmployeeServiceImpl.class);
    @Autowired
    @Qualifier("genericDao")
    private GenericDao genericDao;
    @Autowired
    private UserService userService;
    @Autowired
    @Qualifier(value = "javaMail")
    private JavaMailSender mailSender;
    @Autowired
    private Environment env;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void tagAsDeleted(String encryptedKey) throws CustomException {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("employeeUsername"));
        queryBean.addCondition(new QueryCondition("primaryId", SecurityUtility.decryptId(encryptedKey)));

        Employee employee = (Employee) genericDao.get(queryBean);

        if (employee == null) {
            throw new CustomException("Record does not exist");
        }

        employee.setDeleted(Boolean.TRUE);
        genericDao.saveOrUpdate(employee);

        SecUser employeeUser = employee.getEmployeeUsername();

        employeeUser.setEnabled(Boolean.FALSE);

        /**
         * added this to skip the validation of entered & confirm password.
         */
        employeeUser.setEnteredPassword(employeeUser.getPassword());
        employeeUser.setConfirmPassword(employeeUser.getPassword());

        genericDao.saveOrUpdate(employeeUser);
    }

    @Transactional(readOnly = true)
    @Override
    public Employee findById(Integer id) {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("employeeUsername"));
        queryBean.addCriteria(new QueryCriteria("employeeAvailabilities", FetchMode.JOIN));
        queryBean.addCriteria(new QueryCriteria("employeeLogs", FetchMode.JOIN));

        queryBean.addCondition(new QueryCondition("primaryId", id));
        queryBean.addCondition(new QueryCondition("deleted", Boolean.FALSE));

        Employee employee = (Employee) genericDao.get(queryBean);

        if (employee != null) {
            employee.getEmployeeUsername().setEnteredPassword(employee.getEmployeeUsername().getPassword());
            employee.getEmployeeUsername().setConfirmPassword(employee.getEmployeeUsername().getPassword());
        }

        return employee;
    }

    @Transactional(readOnly = true)
    @Override
    public Employee findByUsername(String username) {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("employeeUsername"));

        queryBean.addCondition(new QueryCondition("employeeUsername.username", username));
        queryBean.addCondition(new QueryCondition("deleted", Boolean.FALSE));

        return (Employee) genericDao.get(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Employee> find(Class<? extends SearchBean> searchBean) {
        QueryBean queryBean = new QueryBean(Employee.class);

        queryBean.addCriteria(new QueryCriteria("employeeLogs", FetchMode.SELECT));

        queryBean.addCondition(new QueryCondition("deleted", Boolean.FALSE));
        queryBean.addOrdering(new QueryOrdering("lastName", QueryOrderType.ASC));

        return genericDao.getListByGeneric(queryBean);
    }

    @Auditable
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdate(Employee employee) {
        SecUser employeeSecUser = employee.getEmployeeUsername();

        employeeSecUser.setEnabled(true);

        if (StringUtils.isEmpty(employeeSecUser.getPassword())
                || (!StringUtils.isEmpty(employeeSecUser.getEnteredPassword()) && employeeSecUser.getEnteredPassword().equals(employeeSecUser.getConfirmPassword()))) {
            employeeSecUser.setPassword(SecurityUtility.encryptPassword(employeeSecUser.getEnteredPassword()));
            genericDao.saveOrUpdate(employeeSecUser);
        }

        if (StringUtils.isEmpty(employee.getEncryptedKey())) {
            genericDao.saveOrUpdate(new SecUserRole(new LookupUserRole(2), employeeSecUser));
        }

        if (StringUtils.isEmpty(SecurityUtility.getCurrentUsername())) {
            employee.setCreatedBy(employeeSecUser);
        }

        genericDao.saveOrUpdate(employee);

        for (EmployeeAvailability e : employee.getEmployeeAvailabilities()) {
            if (e.getEmployee() == null) {
                e.setEmployee(employee);
            }

            if (StringUtils.isEmpty(e.getEncryptedKey())) {
                e.setCreatedBy(employeeSecUser);
            }

            genericDao.saveOrUpdate(e);
        }

        if (employee.getUpdatedBy() == null) {
            try {
                MimeMessage message = mailSender.createMimeMessage();

                MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");

                List<UserBean> superAdmins = userService.retrieveAllSuperAdmin();
                List<String> tos = new ArrayList<>();

                for (UserBean user : superAdmins) {
                    if (!StringUtils.isEmpty(user.getEmail())) {
                        tos.add(user.getEmail());
                    }
                }

                helper.setTo(tos.toArray(new String[]{}));

                message.setSubject("Employee Registration Notification");
                message.setFrom(new InternetAddress(env.getProperty("no-reply@e-tracking.com"), "e-Tracking Application"));

                StringBuilder sb = new StringBuilder();

                sb.append("<h4>New registered employee details</h4><br>");

                sb.append("<strong>Last name:</strong> ");
                sb.append(employee.getLastName());
                sb.append("<br>");
                sb.append("<strong>First name:</strong> ");
                sb.append(employee.getFirstName());
                sb.append("<br>");
                sb.append("<strong>Contact No:</strong> ");
                sb.append(employee.getContactNo());
                sb.append("<br>");
                sb.append("<strong>Security License No:</strong> ");
                sb.append(employee.getSecurityLicenseNo());
                sb.append("<br>");
                sb.append("<strong>First Aid License No:</strong> ");
                sb.append(employee.getFirstAidLicenseNo());

                helper.setText(sb.toString(), true);

                mailSender.send(message);

                LOGGER.debug("Email was successfully sent!");
            } catch (Exception ex) {
                LOGGER.error("Sending email failed!", ex);
            }
        }
    }
}
