package rpo.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rpo.annotations.Auditable;
import rpo.bean.SearchBean;
import rpo.dao.GenericDao;
import rpo.enums.LogStatusEnum;
import rpo.exception.CustomException;
import rpo.model.EmployeeLog;
import rpo.query.QueryBean;
import rpo.query.QueryCondition;
import rpo.query.QueryConditionType;
import rpo.query.QueryCriteria;
import rpo.query.QueryOrderType;
import rpo.query.QueryOrdering;
import rpo.service.EmployeeLogService;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Service
public class EmployeeLogServiceImpl implements EmployeeLogService {

    @Autowired
    @Qualifier("genericDao")
    private GenericDao genericDao;

    @Override
    public void tagAsDeleted(String encryptedKey) throws CustomException {

    }

    @Transactional(readOnly = true)
    @Override
    public EmployeeLog findById(Integer id) {
        QueryBean queryBean = new QueryBean(EmployeeLog.class);

        queryBean.addCriteria(new QueryCriteria("employee"));

        queryBean.addCondition(new QueryCondition("primaryId", id));
        queryBean.addCondition(new QueryCondition("deleted", Boolean.TRUE, QueryConditionType.NOT_EQUAL));

        return (EmployeeLog) genericDao.get(queryBean);
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeLog> find(Class<? extends SearchBean> searchBean) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveOrUpdate(EmployeeLog entity) {
        if (StringUtils.isEmpty(entity.getEncryptedKey())) {
            entity.setLogStatus(LogStatusEnum.UNPAID.getId());
        }

        genericDao.saveOrUpdate(entity);
    }

    @Auditable
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void cancelLog(String encryptedKey) {
        EmployeeLog employeeLog = findById(SecurityUtility.decryptId(encryptedKey));

        if (employeeLog != null) {
            employeeLog.setLogStatus(LogStatusEnum.CANCELLED.getId());
            genericDao.saveOrUpdate(employeeLog);
        }
    }

    @Auditable
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteLog(String encryptedKey) {
        EmployeeLog employeeLog = findById(SecurityUtility.decryptId(encryptedKey));

        if (employeeLog != null) {
            employeeLog.setDeleted(true);
            genericDao.saveOrUpdate(employeeLog);
        }
    }

    @Auditable
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void payLog(String encryptedKey) {
        EmployeeLog employeeLog = findById(SecurityUtility.decryptId(encryptedKey));

        if (employeeLog != null) {
            employeeLog.setLogStatus(LogStatusEnum.PAID.getId());
            employeeLog.setPaidDatetime(new Date());

            genericDao.saveOrUpdate(employeeLog);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<EmployeeLog> filterLogs(String employeeId, String filterBy) {
        QueryBean queryBean = new QueryBean(EmployeeLog.class);

        queryBean.addCriteria(new QueryCriteria("employee"));

        queryBean.addCondition(new QueryCondition("employee.primaryId", SecurityUtility.decryptId(employeeId)));
        queryBean.addCondition(new QueryCondition("deleted", false));

        if (!StringUtils.isEmpty(filterBy) && !"all".equals(filterBy)) {

            if ("last-6-months".equals(filterBy)) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, -6);
                queryBean.addCondition(new QueryCondition("startDatetime", cal.getTime(), QueryConditionType.GE));
            } else {
                String condition = null;

                switch (filterBy) {
                    case "today":
                        condition = "DAY(start_datetime)=DAY(NOW())";
                        break;
                    case "this-week":
                        condition = "WEEKOFYEAR(start_datetime)=WEEKOFYEAR(NOW())";
                        break;
                    case "last-week":
                        condition = "WEEKOFYEAR(start_datetime)=WEEKOFYEAR(NOW()) - 1";
                        break;
                    case "this-month":
                        condition = "MONTH(start_datetime)=MONTH(NOW())";
                        break;
                    case "last-month":
                        condition = "MONTH(start_datetime)=MONTH(NOW()) - 1";
                        break;
                }

                if (condition != null) {
                    QueryCondition queryCondition = new QueryCondition();
                    queryCondition.setConditionType(QueryConditionType.SQL);
                    queryCondition.setValue(condition);
                    queryBean.addCondition(queryCondition);
                }
            }
        }

        queryBean.addOrdering(new QueryOrdering("startDatetime", QueryOrderType.DESC));

        return genericDao.getListByGeneric(queryBean);
    }
}
