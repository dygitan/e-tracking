package rpo.service;

import rpo.model.Employee;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public interface EmployeeService extends GenericService<Employee> {

    Employee findByUsername(String username);
}
