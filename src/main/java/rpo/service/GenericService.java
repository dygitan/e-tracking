package rpo.service;

import java.util.List;
import rpo.bean.SearchBean;
import rpo.exception.CustomException;

/**
 *
 * @author Patrick Tan
 * @param <T>
 * @since Dec 6, 2014
 */
public interface GenericService<T> {

    void tagAsDeleted(String encryptedKey) throws CustomException;

    T findById(Integer id);

    List<T> find(Class<? extends SearchBean> searchBean);

    void saveOrUpdate(T entity);
}
