package rpo.service;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Patrick Tan
 * @since Dec 3, 2014
 */
public class RpoUtility {

    public static String printMe(Object o) {
        if (o == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        
        Field[] fields = o.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            sb.append("field name[");
            sb.append(field.getName());
            sb.append("]");
            sb.append("field value[");

            try {
                sb.append(field.get(o));
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                ex.printStackTrace(System.out);
                sb.append("ERROR");
            }

            sb.append("]");
            sb.append(System.lineSeparator());
        }

        return sb.toString();
    }
}
