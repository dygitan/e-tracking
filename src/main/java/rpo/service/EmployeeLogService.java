package rpo.service;

import java.util.List;
import rpo.model.EmployeeLog;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public interface EmployeeLogService extends GenericService<EmployeeLog> {

    void cancelLog(String encryptedKey);

    void deleteLog(String encryptedKey);

    void payLog(String encryptedKey);

    List<EmployeeLog> filterLogs(String employeeId, String filterBy);
}
