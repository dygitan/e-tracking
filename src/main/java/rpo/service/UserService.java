package rpo.service;

import java.util.List;
import org.springframework.security.core.userdetails.UserDetailsService;
import rpo.bean.PasswordBean;
import rpo.bean.UserBean;
import rpo.model.SecUser;

/**
 *
 * @author Patrick Tan
 * @since Dec 6, 2014
 */
public interface UserService extends UserDetailsService {

    void changePassword(PasswordBean passwordBean);

    SecUser findByUsername(String username);

    UserBean getUserBeanByUsername(String username);

    List<UserBean> retrieveAll();

    List<UserBean> retrieveAllSuperAdmin();

    void saveOrUpdate(UserBean secUser);
}
