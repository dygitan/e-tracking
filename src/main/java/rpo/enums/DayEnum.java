package rpo.enums;

import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public enum DayEnum {

    MONDAY(1, "Monday"),
    TUESDAY(2, "Tuesday"),
    WEDNESDAY(3, "Wednesday"),
    THURSDAY(4, "Thursday"),
    FRIDAY(5, "Friday"),
    SATURDAY(6, "Saturday"),
    SUNDAY(7, "Sunday");
    private int id;
    private String label;

    private DayEnum(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getEncryptedKey() {
        return SecurityUtility.encryptKey(id);
    }

    public static String getLabel(int id) {
        switch (id) {
            case 1:
                return MONDAY.label;
            case 2:
                return TUESDAY.label;
            case 3:
                return WEDNESDAY.label;
            case 4:
                return THURSDAY.label;
            case 5:
                return FRIDAY.label;
            case 6:
                return SATURDAY.label;
            case 7:
                return SUNDAY.label;
            default:
                return null;
        }
    }
}
