package rpo.enums;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public enum LogStatusEnum {

    CANCELLED("C", "Cancelled"),
    PAID("P", "Paid"),
    UNPAID("U", "Unpaid");

    private String id;
    private String label;

    private LogStatusEnum(String id, String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    public static String getLabel(String id) {
        if (CANCELLED.getId().equals(id)) {
            return CANCELLED.getLabel();
        } else if (PAID.getId().equals(id)) {
            return PAID.getLabel();
        } else if (UNPAID.getId().equals(id)) {
            return UNPAID.getLabel();
        } else {
            return null;
        }
    }
}
