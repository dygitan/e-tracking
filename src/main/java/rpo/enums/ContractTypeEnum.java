package rpo.enums;

/**
 *
 * @author Patrick Tan
 * @since Dec 7, 2014
 */
public enum ContractTypeEnum {

    CONTRACTUAL(1, "Contractual"),
    PROJECT_BASED(2, "Project Based"),
    REGULAR(3, "Regular"),
    RELIEVER(4, "Reliever"),
    TRAINEE(5, "Trainee");

    private int id;
    private String label;

    private ContractTypeEnum(int id, String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}
