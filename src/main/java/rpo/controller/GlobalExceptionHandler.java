package rpo.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(GlobalExceptionHandler.class);

//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    public ModelAndView handleException(NoSuchRequestHandlingMethodException ex) {
//        LOGGER.debug("handle exception a ...");
//        ModelAndView mav = new ModelAndView();
//        return mav;
//    }
//
//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    public ModelAndView handleExceptiond(NoHandlerFoundException ex) {
//        LOGGER.debug("handle exception b ...");
//        ModelAndView mav = new ModelAndView();
//        return mav;
//    }
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    @ExceptionHandler(NoHandlerFoundException.class)
//    public String handleConflict() {
//        return "404";
//    }
//
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    @ExceptionHandler(NoSuchRequestHandlingMethodException.class)
//    public void handlesdConflict() {
//        LOGGER.debug("handle exception d ...");
//    }
}
