package rpo.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import rpo.bean.PasswordBean;
import rpo.bean.ResponseBean;
import rpo.model.Employee;
import rpo.model.SecUser;
import rpo.service.EmployeeService;
import rpo.service.UserService;
import rpo.utility.AppUtility;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Controller
@RequestMapping("/secured/*")
public class CommonController {

    private static final Logger LOGGER = Logger.getLogger(CommonController.class);
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private UserService userService;

    @RequestMapping
    public String dashboard() {
        return "dashboard";
    }

    @RequestMapping("profile")
    public String profile(ModelMap model) {
        Employee employee = employeeService.findByUsername(SecurityUtility.getCurrentUsername());
        model.addAttribute("employee", employee);
        model.addAttribute("email", SecurityUtility.getCurrentUser().getSecUser().getEmail());
        return "profile";
    }

    @RequestMapping("log/calculate/{start}/{end}")
    @ResponseBody
    public ResponseBean calculate(@PathVariable(value = "start") String start, @PathVariable(value = "end") String end) {
        ResponseBean responseBean = new ResponseBean();
        responseBean.getData().put("total", AppUtility.calculateDiff(AppUtility.toDate(start), AppUtility.toDate(end)));
        return responseBean;
    }

    @RequestMapping(value = "change-password", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean changePassword(@RequestBody @Validated PasswordBean passwordBean, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        SecUser currentUser = userService.findByUsername(SecurityUtility.getCurrentUsername());

        boolean validCurrentPassword = SecurityUtility.isPasswordMatched(passwordBean.getCurrentPassword(), currentUser.getPassword());

        if (!validCurrentPassword) {
            binding.addError(new FieldError("passwordBean", "currentPassword", "Current password entered is invalid."));
        }

        if (!binding.hasErrors() && validCurrentPassword) {
            userService.changePassword(passwordBean);
            responseBean.setMessage("Password was successfully changed.");
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;
    }
}
