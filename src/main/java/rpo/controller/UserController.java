package rpo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import rpo.bean.ResponseBean;
import rpo.bean.UserBean;
import rpo.model.SecUser;
import rpo.service.UserService;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Controller
@RequestMapping("/secured/user/*")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("view/{id}")
    public String view(ModelMap model, @PathVariable String id) {
        Integer primaryId = SecurityUtility.decryptId(id);

        if ("all".equalsIgnoreCase(id)) {
            return "viewUsers";
        } else if (primaryId > 0) {
            return "manageUser";
        } else {
            return "redirect:/secured/user/view/all";
        }
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean save(@RequestBody @Validated UserBean user, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            userService.saveOrUpdate(user);
            responseBean.getData().put("users", userService.retrieveAll());
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;
    }

    @ResponseBody
    @RequestMapping("json/all")
    public List<UserBean> getJsonEmployees() {
        return userService.retrieveAll();
    }

    @ResponseBody
    @RequestMapping("json/{username}")
    public UserBean getJsonEmployeeLogs(@PathVariable String username) {
        return userService.getUserBeanByUsername(username);
    }
}
