package rpo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Controller
public class UnsecureController {

    @RequestMapping("/*")
    public String index() {
        return "redirect:secured/dashboard";
    }

//    @RequestMapping("unsecure/login")
//    public String login() {
//        return "login";
//    }
}
