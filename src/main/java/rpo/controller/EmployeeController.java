package rpo.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.groups.Default;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import rpo.bean.EmployeeBean;
import rpo.bean.PropertyEditorBean;
import rpo.bean.ResponseBean;
import rpo.exception.CustomException;
import rpo.model.Employee;
import rpo.model.EmployeeLog;
import rpo.model.SecUser;
import rpo.service.EmployeeLogService;
import rpo.service.EmployeeService;
import rpo.utility.MessageUtil;
import rpo.utility.SecurityUtility;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Controller
@RequestMapping("/secured/employee/*")
public class EmployeeController {

    private static final Logger LOGGER = Logger.getLogger(EmployeeController.class);
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeLogService employeeLogService;

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        LOGGER.info("init binder param: " + request.getParameter("employee"));

        if (!StringUtils.isEmpty(request.getParameter("employee"))) {
            PropertyEditorBean bean = new PropertyEditorBean(Employee.class);
            binder.registerCustomEditor(Employee.class, "employee", bean);
        }
    }

    @RequestMapping("add")
    public String add(ModelMap model, @RequestParam(required = false, value = "mode") String mode) {
        Employee employee = new Employee();

        if ("test".equals(mode)) {

            int random = new Random().nextInt();

            employee.setLastName("last_" + random);
            employee.setFirstName("first_" + random);
            employee.setContactNo("contact_" + random);
            employee.setAddress("address_" + random);

            employee.setFirstAidLicenseExpiry(new Date());
            employee.setFirstAidLicenseNo("first_" + random);

            employee.setSecurityLicenseExpiry(new Date());
            employee.setSecurityLicenseNo("sec_" + random);

            SecUser secUser = new SecUser("user" + random);
            secUser.setConfirmPassword("123456");
            secUser.setEnteredPassword("123456");
            employee.setEmployeeUsername(secUser);
        }

        model.addAttribute("employee", employee);
        return "manageEmployee";
    }

    @RequestMapping(value = "delete/{encryptedKey}")
    @ResponseBody
    public ResponseBean delete(@PathVariable String encryptedKey) {
        ResponseBean responseBean = new ResponseBean();

        try {
            employeeService.tagAsDeleted(encryptedKey);
            responseBean.setMessage(MessageUtil.RECORD_DELETED);
        } catch (CustomException e) {
            responseBean.setMessage(e.getMessage());
        }

        return responseBean;
    }

    @RequestMapping("log")
    public String log(ModelMap model) {
        model.addAttribute("employee", employeeService.findByUsername(SecurityUtility.getCurrentUsername()));
        return "logHours";
    }

    @RequestMapping(value = "log/{actionType}")
    @ResponseBody
    public ResponseBean processLog(HttpServletRequest request, @PathVariable String actionType) {
        String encryptedKey = request.getParameter("log");

        if (null != actionType) {
            switch (actionType) {
                case "cancel":
                    employeeLogService.cancelLog(encryptedKey);
                    break;
                case "pay":
                    employeeLogService.payLog(encryptedKey);
                    break;
                case "delete":
                    employeeLogService.deleteLog(encryptedKey);
                    break;
            }
        }

        ResponseBean responseBean = new ResponseBean();
        responseBean.getData().put("logs",
                employeeLogService.filterLogs(request.getParameter("employee"),
                        request.getParameter("filter")));

        return responseBean;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public ResponseBean save(@ModelAttribute("data") EmployeeBean employeeBean) {
        ResponseBean responseBean = new ResponseBean();

        Employee employee = employeeBean.getEmployee();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Set<ConstraintViolation<Employee>> errors = factory.getValidator().validate(employee, Default.class);

        List<FieldError> fieldErrors = null;

        boolean isPasswordValid = employee.getEmployeeUsername().isValid();

        if (!isPasswordValid) {
            fieldErrors = new ArrayList<>();
            fieldErrors.add(new FieldError("employee", "employeeUsername.enteredPassword", "the password entered did not matched."));
        }

        if (!CollectionUtils.isEmpty(errors)) {

            for (ConstraintViolation<Employee> e : errors) {
                if ("employeeUsername.enteredPassword".equals(e.getPropertyPath().toString()) && isPasswordValid) {
                    continue;
                }

                if (fieldErrors == null) {
                    fieldErrors = new ArrayList<>();
                }

                fieldErrors.add(new FieldError("employee", e.getPropertyPath().toString(), e.getMessage()));
            }
        }

        responseBean.setErrors(fieldErrors);

        if (CollectionUtils.isEmpty(responseBean.getErrors())) {
            try {
                Employee oldEmployee = employeeService.findById(employee.getPrimaryId());

                if (employeeBean.getSecurityLicenseUpload() != null) {
                    employee.setSecurityLicenseFile(employeeBean.getSecurityLicenseUpload().getBytes());
                    employee.setSecurityLicenseFileType(employeeBean.getSecurityLicenseUpload().getContentType());
                } else if (oldEmployee != null) {
                    employee.setSecurityLicenseFile(oldEmployee.getSecurityLicenseFile());
                    employee.setSecurityLicenseFileType(oldEmployee.getSecurityLicenseFileType());
                }

                if (employeeBean.getFirstAidLicenseUpload() != null) {
                    employee.setFirstAidLicenseFile(employeeBean.getFirstAidLicenseUpload().getBytes());
                    employee.setFirstAidLicenseFileType(employeeBean.getFirstAidLicenseUpload().getContentType());
                } else if (oldEmployee != null) {
                    employee.setFirstAidLicenseFile(oldEmployee.getFirstAidLicenseFile());
                    employee.setFirstAidLicenseFileType(oldEmployee.getFirstAidLicenseFileType());
                }
            } catch (IOException ex) {
                LOGGER.error(ex);
            }

            employeeService.saveOrUpdate(employee);

            responseBean.getData().put("encryptedKey", employee.getEncryptedKey());
            responseBean.getData().put("createdBy", employee.getCreatedBy());
            responseBean.getData().put("createdDate", employee.getCreatedDate());

            if (employee.getUpdatedBy() == null) {
                responseBean.setMessage(MessageUtil.RECORD_ADDED);
            } else {
                responseBean.setMessage(MessageUtil.RECORD_UPDATED);
            }
        }

        return responseBean;
    }

    @RequestMapping(value = "save/log", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean saveLog(@RequestBody @Validated EmployeeLog entity, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            employeeLogService.saveOrUpdate(entity);

            responseBean.getData().put("logs",
                    employeeLogService.filterLogs(entity.getEmployee().getEncryptedKey(), null));
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;
    }

    @RequestMapping("view/{id}")
    public String view(ModelMap model, @PathVariable String id) {
        Integer primaryId = SecurityUtility.decryptId(id);

        if ("all".equalsIgnoreCase(id)) {
            return "viewEmployees";
        } else if (primaryId > 0) {
            model.addAttribute("employee", employeeService.findById(primaryId));
            return "manageEmployee";
        } else {
            return "redirect:/secured/employee/view/all";
        }
    }

    @RequestMapping(value = "view/attachment/{type}/{key}")
    public void viewAttachment(@PathVariable(value = "type") String attachmentType,
            @PathVariable(value = "key") String encryptedKey,
            HttpServletResponse response) {
        Employee employee = employeeService.findById(SecurityUtility.decryptId(encryptedKey));

        if (employee != null) {

            switch (attachmentType) {
                case "first-aid":
                    generateAttachment(response, employee.getFirstAidLicenseFileType(),
                            employee.getFirstAidLicenseFile());
                    break;
                case "security":
                    generateAttachment(response, employee.getSecurityLicenseFileType(),
                            employee.getSecurityLicenseFile());
                    break;
            }
        }
    }

    private void generateAttachment(HttpServletResponse response, String fileType, byte[] contents) {
        response.setContentType(fileType);

        try {
            OutputStream out = response.getOutputStream();
            out.write(contents);
        } catch (IOException ex) {
            LOGGER.error(ex);
        }
    }

    @ResponseBody
    @RequestMapping("json/all")
    public List<Employee> getJsonEmployees() {
        return employeeService.find(null);
    }

    @ResponseBody
    @RequestMapping("json/logs/{id}/{filterBy}")
    public List<EmployeeLog> getJsonEmployeeLogs(@PathVariable String id, @PathVariable String filterBy) {
        return employeeLogService.filterLogs(id, filterBy);
    }
}
