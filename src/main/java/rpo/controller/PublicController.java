package rpo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import rpo.bean.ResponseBean;
import rpo.model.Employee;
import rpo.service.EmployeeService;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
@Controller
@RequestMapping("/public/*")
public class PublicController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("login")
    public String login() {
        return "login";
    }

    @RequestMapping("signup")
    public String signup() {
        return "signUpForm";
    }

    @RequestMapping(value = "signup/submit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseBean save(@RequestBody @Validated Employee entity, BindingResult binding) {
        ResponseBean responseBean = new ResponseBean();

        if (!binding.hasErrors()) {
            employeeService.saveOrUpdate(entity);

            responseBean.getData().put("encryptedKey", entity.getEncryptedKey());
            responseBean.getData().put("createdBy", entity.getCreatedBy());
            responseBean.getData().put("createdDate", entity.getCreatedDate());
        } else {
            responseBean.setErrors(binding.getFieldErrors());
        }

        return responseBean;

    }
}
