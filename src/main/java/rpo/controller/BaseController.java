package rpo.controller;

import java.util.HashMap;
import java.util.Map;
import rpo.json.transformer.JsonConverter;

/**
 *
 * @author Patrick Tan (patricktan2014@yahoo.com)
 */
public class BaseController {

    protected String success(String message) {
        return success(message, null);
    }

    protected String success(String message, Object data) {
        Map map = new HashMap();

        map.put("success", true);
        map.put("data", data);
        map.put("message", message);

        return JsonConverter.serialize(map);
    }
}
