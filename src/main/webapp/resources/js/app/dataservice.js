var dataService = angular.module('dataService', ['ngResource']);

dataService.factory('dataService', function ($http) {
    return {
        query: function ($scope) {
            var url;

            if ($scope.url) {
                url = $scope.url;
            } else {
                url = '/secured/' + $scope.module + '/json/all';
            }

            $http.get(url, {params: $scope.params})
                    .then(function (response) {
                        $scope.items = response.data;
                        $scope.showLoading = false;

                        if (isDebug) {
                            console.log('Response items length: ' + $scope.items.length);
                        }
                    });
        }
    };
});