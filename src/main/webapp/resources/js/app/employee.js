$(document).ready(function () {
    $('.datetimepicker').on('dp.hide', function () {
        var start = $('#startDateTime').val();
        var end = $('#endDateTime').val();


        $.ajax({
            url: baseUrl + '/secured/log/calculate/' + start + '/' + end,
            success: function (response, textStatus, jqXHR) {
                $('#totalLoggedHours').val(response.data.total);
            }
        });
    });

    $('.timepicker').on('dp.hide', function () {
        $(this).parent().find('input.free-time').val(new moment().format('YYYY-MM-DD ') + $(this).val());
    });

    $('input.available-option').on('change', function () {
        if ($(this).attr('checked')) {
            $(this).removeAttr('checked');
        } else {
            $(this).attr('checked', 'checked');
        }
    });
});

var app = angular.module('employeeModule', [
    'employeeCtrl',
    'dataService'
]);

var employeeCtrl = angular.module('employeeCtrl', []);

employeeCtrl.controller('logHourCtrl', [
    '$scope',
    '$http',
    'dataService',
    function ($scope, $http, dataService) {
        $scope.save = function () {
            if (!$scope.employeeLog) {
                $scope.employeeLog = {};
            }

            if (!$scope.employeeLog.employee) {
                $scope.employeeLog.employee = {
                    encryptedKey: $scope.employee.encryptedKey
                };
            }

            $scope.employeeLog.startDatetime = $('#startDateTime').val();
            $scope.employeeLog.endDatetime = $('#endDateTime').val();
            $scope.employeeLog.endDatetime = $('#endDateTime').val();

            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var request = $http.post('/secured/employee/save/log',
                    JSON.stringify($scope.employeeLog),
                    {blockUI: true}
            );

            request.success(function (response, status) {
                if (isDebug) {
                    console.log('response status [' + status + ']');
                    console.log(response);
                }

                if (!response.errors) {
                    $scope.errors = {};

                    $scope.items = response.data.logs;
                    var employee = $scope.employeeLog.employee;

                    $scope.employeeLog = {};
                    $scope.employeeLog.employee = employee;

                    $('#totalLoggedHours').val('');
                } else {
                    $scope.errors = response.errors;
                }
            });
        };

        $scope.initLogs = function () {
            $scope.url = '/secured/employee/json/logs/' + $scope.employee.encryptedKey;
            $scope.showLoading = true;
            dataService.query($scope);
        };

        $scope.reset = function () {
            var employee = $scope.employeeLog.employee;

            $scope.employeeLog = {};
            $scope.employeeLog.employee = employee;
        };
    }
]);

employeeCtrl.controller('viewLogHoursCtrl', [
    '$scope',
    '$http',
    'dataService',
    function ($scope, $http, dataService) {
        $scope.filterBy = {
            id: 'all',
            label: 'All'
        };

        $scope.filter = function (filter) {
            $scope.filterBy = filter;

            $scope.items = {};
            $scope.url = '/secured/employee/json/logs/' + $scope.employee.encryptedKey + '/' + filter.id;
            $scope.showLoading = true;
            dataService.query($scope);
        };

        $scope.initLogs = function () {
            $scope.url = '/secured/employee/json/logs/' + $scope.employee.encryptedKey + '/all';
            $scope.showLoading = true;
            dataService.query($scope);
        };

        $scope.checkAll = function () {
            var checked;

            if ($('#checkAllLogs:checked').val() === undefined) {
                checked = false;
            } else {
                checked = true;
            }

            $('#tblLogHours tbody > tr').each(function () {
                $(this).find('td:first > span > input[type=checkbox]').prop('checked', checked);
            });
        };

        $scope.cancel = function () {
            var selectedLoggedHours = getSelectedLogs();

            $('#paymentModal').find('input[name=actionType]').val('cancel');

            $('#paymentModalLabel').html('PAYMENT CANCELLATION');

            if (selectedLoggedHours.length > 0) {
                $('#promptMessage').html('Are you sure you want to proceed?');
            } else {
                $('#paymentModal').find('.modal-footer').hide();
                $('#promptMessage').html('Please select the logged hour/s you want to cancel.');
            }

            $('#paymentModal').modal('show');
        };

        $scope.pay = function () {
            var selectedLoggedHours = getSelectedLogs();
            $('#paymentModal').find('input[name=actionType]').val('pay');

            $('#paymentModalLabel').html('PAYMENT CONFIRMATION');

            if (selectedLoggedHours.length > 0) {
                $('#promptMessage').html('Are you sure you want to proceed?');
            } else {
                $('#paymentModal').find('.modal-footer').hide();
                $('#promptMessage').html('Please select the logged hour/s you want to pay.');
            }

            $('#paymentModal').modal('show');
        };

        $scope.delete = function () {
            var selectedLoggedHours = getSelectedLogs();
            $('#paymentModal').find('input[name=actionType]').val('delete');

            $('#paymentModalLabel').html('DELETE CONFIRMATION');

            if (selectedLoggedHours.length > 0) {
                $('#promptMessage').html('Are you sure you want to proceed?');
            } else {
                $('#paymentModal').find('.modal-footer').hide();
                $('#promptMessage').html('Please select the logged hour/s you want to delete.');
            }

            $('#paymentModal').modal('show');
        };

        $scope.confirm = function () {
            var selectedLogs = getSelectedLogs();
            var actionType = $('#paymentModal').find('input[name=actionType]').val();
            var url;

            if ('pay' == actionType) {
                url = '/secured/employee/log/pay';
            } else if ('cancel' == actionType) {
                url = '/secured/employee/log/cancel';
            } else if ('delete' == actionType) {
                url = '/secured/employee/log/delete';
            }

            for (var i in selectedLogs) {
                $http.get(url, {
                    blockUI: true,
                    params: {
                        log: selectedLogs[i],
                        employee: $scope.employee.encryptedKey,
                        filter: $scope.filterBy.id
                    }
                }).success(function (response) {
                    $scope.items = response.data.logs;
                    $('#paymentModal').modal('hide');
                });
            }
        };
    }
]);
employeeCtrl.controller('viewEmployeeCtrl', [
    '$scope',
    '$http',
    'dataService',
    function ($scope, $http, dataService) {
        $scope.showLoading = true;
        $scope.module = 'employee';

        dataService.query($scope);

        $scope.delete = function ($event) {
            var employee = $(this)[0].employee;

            $scope.encryptedKey = employee.encryptedKey;
            $scope.employeeName = employee.lastName + ', ' + employee.firstName;

            $('#confirmModal').modal();

            $event.preventDefault();
        };

        $scope.confirmDeletion = function () {
            $http.get('/secured/employee/delete/' + $scope.encryptedKey)
                    .success(function () {
                        $('a[id="' + $scope.encryptedKey + '"').fadeOut();
                        $('a[id="' + $scope.encryptedKey + '"').remove();

                        $('#confirmModal').hide();
                        showPrompt('Record was successfully deleted.');
                    });
        };
    }
]);

employeeCtrl.controller('signupCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.clear = function () {
            $scope.employee = {};
        };

        $scope.save = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            $scope.employee.securityLicenseExpiry = $('#securityExpiryDate').val();
            $scope.employee.firstAidLicenseExpiry = $('#firstAidExpiryDate').val();

            var request = $http.post('/public/signup/submit',
                    JSON.stringify($scope.employee),
                    {blockUI: true}
            );

            request.success(function (response, status) {
                if (!response.errors) {
                    $scope.errors = {};

                    $scope.employee.encryptedKey = response.data.encryptedKey;
                    $scope.employee.createdBy = response.data.createdBy;
                    $scope.employee.createdDate = response.data.createdDate;

                    $('#blockPopup span.message').html('Your account has been successfully created.<br> You may now sign in!');

                    $('#btnCloseModal').html('Sign in  <span class="glyphicon glyphicon-log-in"></span>');
                    $('#btnCloseModal').on('click', function () {
                        window.location.href = baseUrl + '/public/login';
                    });
                } else {
                    $scope.errors = response.errors;
                }
            });
        };
    }
]);

employeeCtrl.controller('manageCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.save = function () {
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            var temp = new Array();

            var currentDate = new moment().format('YYYY-MM-DD ');

            for (var index in $scope.employeeAvailabilities) {
                temp[index] = $scope.employeeAvailabilities[index];

                var freeTime = $('input[name="freeTime' + temp[index].freeDay + '"]').val();

                if (freeTime) {
                    temp[index].freeTime = currentDate + freeTime;
                }

                var freeTimeEnd = $('input[name="freeTimeEnd' + temp[index].freeDay + '"]').val();

                if (freeTimeEnd) {
                    temp[index].freeTimeEnd = currentDate + freeTimeEnd;
                }

                var available = $('input[name="available' + temp[index].freeDay + '"]').attr('checked') ? 1 : 0;

                temp[index].available = available;
            }

            $scope.employee.employeeAvailabilities = new Array();
            $scope.employee.employeeAvailabilities = temp;

            $scope.employee.securityLicenseExpiry = $('#securityExpiryDate').val();
            $scope.employee.firstAidLicenseExpiry = $('#firstAidExpiryDate').val();

            var formData = new FormData();

            if (firstAidFile.files[0]) {
                formData.append('firstAidLicenseUpload', firstAidFile.files[0]);
            }

            if (securityFile.files[0]) {
                formData.append('securityLicenseUpload', securityFile.files[0]);
            }

            formData.append('employee', JSON.stringify($scope.employee));

            $http({
                method: 'POST',
                url: '/secured/employee/save',
                headers: {'Content-Type': undefined},
                data: formData,
                blockUI: true,
                transformRequest: function (data) {
                    return data;
                }
            }).success(function (response, status) {
                if (!response.errors) {
                    $scope.errors = {};

                    $scope.employee.encryptedKey = response.data.encryptedKey;
                    $scope.employee.createdBy.username = response.data.createdBy.username;
                    $scope.employee.createdDate = response.data.createdDate;
                } else {
                    $scope.errors = response.errors;
                }
            });
        };
    }
]);

function processErrors(errors) {
    var temp = {};

    if (isDebug) {
        console.log('process errors');
        console.log(errors);
    }

    for (var index in errors) {
        var error = errors[index];
        temp[error.field] = error.defaultMessage;

        if (isDebug) {
            console.log('error field name [' + error.field + ' - ' + error.defaultMessage + ']');
        }
    }

    return temp;
}

function getSelectedLogs() {
    var selectedValues = new Array();
    var index = 0;

    $('#tblLogHours input[type=checkbox]:checked').each(function () {
        var value = $(this).val();


        console.log(value)

        if (value != undefined) {
            selectedValues[index] = $(this).val();
            index++;
        }
    });

    return selectedValues;
}