var globalModule = angular.module('globalModule', [
    'globalCtrl',
    'employeeModule',
    'userModule'
]);

globalModule.config(function ($httpProvider, $provide) {
    console.log('initiliaze global module config!');

    $provide.factory('httpInterceptor', function ($q, $rootScope) {
        var showBlockUI = false;

        return {
            'request': function (config) {
                if (isDebug) {
                    console.log('httpInterceptor [request]');
                }

                showBlockUI = config.blockUI;

                if (showBlockUI) {
                    blockUI(true);
                }

                config.url = baseUrl + config.url;

                // intercept and change config: e.g. change the URL
                // config.url += '?nocache=' + (new Date()).getTime();
                // broadcasting 'httpRequest' event
                $rootScope.$broadcast('httpRequest', config);

                return config || $q.when(config);
            },
            'response': function (response) {
                if (isDebug) {
                    console.log('httpInterceptor [response]');
                    console.log(response);
                }

                if (response.data.errors && response.data.errors.length > 0) {
                    response.data.errors = processErrors(response.data.errors);
                }

                if (showBlockUI) {
                    if (response.data.errors) {
                        blockUI(false);
                    } else {
                        $('#blockPopup').find('img').hide();
                        $('#blockPopup').find('.button-area').show();
                        $('#blockPopup').find('.message').html('Request successfully completed!');
                    }
                }

                // we can intercept and change response here...
                // broadcasting 'httpResponse' event
                $rootScope.$broadcast('httpResponse', response);

                return response || $q.when(response);
            },
            'requestError': function (rejection) {
                if (isDebug) {
                    console.log('httpInterceptor [request error]');
                }

                // broadcasting 'httpRequestError' event
                $rootScope.$broadcast('httpRequestError', rejection);
                return $q.reject(rejection);
            },
            'responseError': function (rejection) {
                if (isDebug) {
                    console.log('httpInterceptor [response error]');
                }

                if (showBlockUI) {
                    $('#blockPopup').find('.block-popup-container').addClass('danger');
                    $('#blockPopup').find('img').hide();
                    $('#blockPopup').find('.button-area').show();
                    $('#blockPopup').find('.message').html('Whoops! System was not able to process your request!');
                }

                // broadcasting 'httpResponseError' event
                $rootScope.$broadcast('httpResponseError', rejection);
                return $q.reject(rejection);
            }
        };
    });

    $httpProvider.interceptors.push('httpInterceptor');
});

var globalCtrl = angular.module('globalCtrl', []);

globalCtrl.controller('popupCtrl', [
    '$scope',
    function ($scope) {
        $scope.close = function () {
            blockUI(false);
        };
    }
]);

globalCtrl.controller('profileCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
        $scope.clear = function () {
            var email = $scope.loginCredentials.email;
            $scope.loginCredentials = {};
            $scope.loginCredentials.email = email;
        };
        
        $scope.change = function () {
            console.log('clicked change...');

            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            $http.post('/secured/change-password',
                    JSON.stringify($scope.loginCredentials),
                    {blockUI: true}
            ).success(function (response, status) {
                if (!response.errors) {
                    $scope.errors = {};
                } else {
                    $scope.errors = response.errors;
                }
            });
        };
    }
]);