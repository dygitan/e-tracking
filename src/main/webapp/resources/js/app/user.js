
console.log('init user...')
var app = angular.module('userModule', [
    'userCtrl',
    'dataService'
]);

var userCtrl = angular.module('userCtrl', []);

userCtrl.controller('manageUserCtrl', [
    '$scope',
    '$http',
    'dataService',
    function ($scope, $http, dataService) {
        $scope.showLoading = true;
        $scope.module = 'user';

        dataService.query($scope);

        $scope.edit = function ($user) {
            $http.get('/secured/user/json/' + $user.username)
                    .success(function (response) {
                        $scope.user = response;
                    });
        };

        $scope.clear = function () {
            $scope.user = {};
            $scope.errors = {};
        };

        $scope.submit = function () {
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http.defaults.headers.post['X-CSRF-TOKEN'] = csrfToken;

            if (!$scope.user) {
                $scope.user = {};
            }

            $http.post('/secured/user/save',
                    JSON.stringify($scope.user),
                    {blockUI: true})
                    .success(function (response) {
                        if (!response.errors) {
                            $scope.errors = {};
                            $scope.items = response.data.users;
                            $scope.user = {};
                        } else {
                            $scope.errors = response.errors;
                        }
                    });
        };
    }
]);