<%@include file="../common/taglibs.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            e-Tracking | <tiles:insertAttribute name="title" ignore="true" />
        </title>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        
        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap-3.3.1.min.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" href="${baseUrl}/resources/css/style.css" />

        <script src="${baseUrl}/resources/js/jquery-1.11.0.min.js"></script>
        <script src="${baseUrl}/resources/js/bootstrap-3.3.1.min.js"></script>
        
        <script src="${baseUrl}/resources/js/moment.js"></script>
        <script src="${baseUrl}/resources/js/bootstrap-datetimepicker.js"></script>
        

        <script src="${baseUrl}/resources/js/1.3.5/angular.min.js"></script>
        <script src="${baseUrl}/resources/js/1.3.5/angular-resource.min.js"></script>
        <script src="${baseUrl}/resources/js/1.3.5/angular-route.min.js"></script>
        
        <script src="${baseUrl}/resources/js/global.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/app/global.js" type="text/javascript"></script>

        <script src="${baseUrl}/resources/js/app/dataservice.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/app/employee.js" type="text/javascript"></script>
        <script src="${baseUrl}/resources/js/app/user.js" type="text/javascript"></script>
        
        <style>
            body, table {
                font-size: 13px;
                font-family: 'Tahoma', sans-serif
            }
        </style>
    </head>
    <body>
        <tiles:insertAttribute name="navigation" />

        <div class="container-fluid" style="margin-top: 65px" ng-app="globalModule">
            <tiles:insertAttribute name="content" />
            <%@include file="global-modal.jsp" %>
        </div>

        <tiles:insertAttribute name="footer" />
    </body>
</html>
