<%@include file="../common/taglibs.jsp" %>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">e-Tracking</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <security:authorize access="isAnonymous()">
                    <li>
                        <a href="${baseUrl}/public/login">
                            <span class="glyphicon glyphicon-log-in"></span> Login
                        </a>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">

                    <li class="active">
                        <a href="${baseUrl}/secured/dashboard">
                            <span class="glyphicon glyphicon-dashboard"></span> Dashboard
                        </a>
                    </li>
                    <security:authorize access="hasRole('ROLE_EMPLOYEE')">
                        <li>
                            <a href="${baseUrl}/secured/employee/log">Log</a>
                        </li>
                    </security:authorize>

                    <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Employees <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li>
                                    <a href="${baseUrl}/secured/employee/add">Add</a>
                                </li>
                                <li>
                                    <a href="${baseUrl}/secured/employee/view/all">View All</a>
                                </li>
                            </ul>
                        </li>
                    </security:authorize>
                </security:authorize>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <security:authorize access="isAuthenticated()">

                    <li>
                        <a href="${baseUrl}/secured/profile">
                            <span class="glyphicon glyphicon-user"></span> Logged in as  

                            <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                                (Employer)
                            </security:authorize>
                            <security:authorize access="hasRole('ROLE_EMPLOYEE')">
                                (Employee)
                            </security:authorize>

                            <security:authentication property="principal.username" />
                        </a>
                    </li>

                    <security:authorize access="hasRole('ROLE_SUPER_ADMIN')">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="glyphicon glyphicon-cog"></span> User Management <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="${baseUrl}/secured/user/view/all">Users</a>
                                </li>
                            </ul>
                        </li>
                    </security:authorize>

                    <li>
                        <a href="${baseUrl}/logout">
                            <span class="glyphicon glyphicon-log-out"></span> Logout
                        </a>
                    </li>

                </security:authorize>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>