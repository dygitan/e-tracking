<%@include file="taglibs.jsp" %>

<security:authorize access="hasRole('ROLE_EMPLOYEE')">
    <div ng-app="employeeModule">
        <div  class="row" ng-controller="manageCtrl">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">PROFILE</div>
                    <div class="panel-body">
                        <%@include file="employee-info.jsp" %>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success btn-sm" ng-click="save()">SAVE</button>
                                |
                                <button class="btn btn-danger btn-sm">
                                    <a href="${baseUrl}/secured/dashboard">BACK</a>
                                </button>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div>
        </div>
    </div>
</security:authorize>

<security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
    <div >
        <div  class="row" ng-controller="profileCtrl">
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">CHANGE PASSWORD</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="">
                                Email <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control" 
                                       maxlength="45"
                                       ng-model="loginCredentials.email"       
                                       ng-init="loginCredentials.email = '${requestScope.email}'"
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.email" class="ng-hide validation-error">{{errors.email}}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="">
                                Current Password <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="password" 
                                       class="form-control" 
                                       maxlength="45"
                                       ng-model="loginCredentials.currentPassword"                                          
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.currentPassword" class="ng-hide validation-error">{{errors.currentPassword}}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="">
                                New Password <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="password" 
                                       class="form-control" 
                                       maxlength="45"
                                       ng-model="loginCredentials.newPassword"                                          
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.newPassword" class="ng-hide validation-error">{{errors.newPassword}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">
                                Confirm Password <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="password" 
                                       class="form-control" 
                                       maxlength="45"
                                       ng-model="loginCredentials.confirmPassword"                                          
                                       />
                                <div class="text-right">
                                    <span ng-show="!errors.passwordBean.newPassword && errors.valid" class="ng-hide validation-error">{{errors.valid}}</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success btn-sm" ng-click="change()">CHANGE</button>
                                |
                                <button class="btn btn-danger btn-sm" ng-click="clear()">CLEAR</button>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</security:authorize>
