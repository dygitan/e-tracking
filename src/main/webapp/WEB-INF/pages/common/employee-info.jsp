<%@include file="../common/taglibs.jsp" %>

<div class="row">    
    <div class="col-xs-12 col-sm-12 col-md-6">
        <input type="hidden" 
               class="form-control"                                      
               ng-model="employee.encryptedKey"      
               ng-init="employee.encryptedKey = '${requestScope.employee.encryptedKey}'"
               />

        <input type="hidden" 
               class="form-control"                                      
               ng-model="employee.createdBy.username"      
               ng-init="employee.createdBy.username = '${requestScope.employee.createdBy.username}'"
               />
        <input type="hidden" 
               class="form-control"                                      
               ng-model="employee.createdDate"      
               ng-init="employee.createdDate = '${requestScope.employee.createdDate}'"
               />

        <div class="form-group">
            <label for="">
                Last Name <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="45"
                       ng-model="employee.lastName"
                       ng-init="employee.lastName = '${requestScope.employee.lastName}'"                                           
                       />
                <div class="text-right">
                    <span ng-show="errors.lastName" class="ng-hide validation-error">{{errors.lastName}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                First Name <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="45"
                       ng-model="employee.firstName"
                       ng-init="employee.firstName = '${requestScope.employee.firstName}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.firstName" class="ng-hide validation-error">{{errors.firstName}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Contact No <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="45"
                       ng-model="employee.contactNo"
                       ng-init="employee.contactNo = '${requestScope.employee.contactNo}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.contactNo" class="ng-hide validation-error">{{errors.contactNo}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Address <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <textarea
                    class="form-control" 
                    ng-model="employee.address"
                    ng-init="employee.address = '${requestScope.employee.address}'"></textarea>
                <div class="text-right">
                    <span ng-show="errors.address" class="ng-hide validation-error">{{errors.address}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                LOGIN CREDENTIALS 
            </label>
            <hr>
        </div>
        <div class="form-group">
            <label for="">
                Username <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="10"
                       ng-model="employee.employeeUsername.username"
                       ng-init="employee.employeeUsername.username = '${requestScope.employee.employeeUsername.username}'"
                       ${not empty requestScope.employee.encryptedKey ? 'disabled' : ''}
                       />
                <div class="text-right">
                    <span ng-show="errors['employeeUsername.username']" class="ng-hide validation-error">{{errors['employeeUsername.username']}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>
                Password <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="password" class="hidden" 
                       ng-model="employee.employeeUsername.password"
                       ng-init="employee.employeeUsername.password = '${requestScope.employee.employeeUsername.password}'"
                       />
                <input type="password" 
                       class="form-control" 
                       maxlength="10"
                       ng-model="employee.employeeUsername.enteredPassword"
                       />
                <div class="text-right">
                    <span ng-show="errors['employeeUsername.enteredPassword']" class="ng-hide validation-error">{{errors['employeeUsername.enteredPassword']}}</span>

                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Confirm Password <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="password" 
                       class="form-control" 
                       ng-model="employee.employeeUsername.confirmPassword"                                       
                       />
                <div class="text-right">
                    <span ng-show="!errors['employeeUsername.enteredPassword'] && errors['employeeUsername.valid']" class="ng-hide validation-error">{{errors['employeeUsername.valid']}}</span>
                </div>
            </div>
        </div>
    </div>   
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="">
                Security License No <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="45"
                       ng-model="employee.securityLicenseNo"
                       ng-init="employee.securityLicenseNo = '${requestScope.employee.securityLicenseNo}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.securityLicenseNo" class="ng-hide validation-error">{{errors.securityLicenseNo}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Expiry Date <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       id="securityExpiryDate"
                       class="form-control datepicker" 
                       maxlength="10"
                       ng-model="employee.securityLicenseExpiry"
                       ng-init="employee.securityLicenseExpiry = '${requestScope.employee.securityLicenseExpiry}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.securityLicenseExpiry" class="ng-hide validation-error">{{errors.securityLicenseExpiry}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Upload Security License No <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="file" 
                       id="securityFile"
                       name="securityFile"
                       accept="application/pdf,image/*"
                       class="form-control" 
                       />        

            </div>
            <c:if test="${not empty requestScope.employee.securityLicenseFile}">
                <div class="input-group-sm" style="text-align: right; margin-top: 5px">



                    <button class="btn-attachment" type="button" onclick="window.open('${baseUrl}/secured/employee/view/attachment/security/${requestScope.employee.encryptedKey}', '_blank');">
                        View Attachment <span class="glyphicon glyphicon-paperclip"></span>
                    </button>
                </div>
            </c:if>
        </div>
        <hr>
        <div class="form-group">
            <label for="">
                First Aid License No <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       class="form-control" 
                       maxlength="45"
                       ng-model="employee.firstAidLicenseNo"
                       ng-init="employee.firstAidLicenseNo = '${requestScope.employee.firstAidLicenseNo}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.firstAidLicenseNo" class="ng-hide validation-error">{{errors.firstAidLicenseNo}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Expiry Date <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="text" 
                       id="firstAidExpiryDate"
                       class="form-control datepicker" 
                       maxlength="10"
                       ng-model="employee.firstAidLicenseExpiry"
                       ng-init="employee.firstAidLicenseExpiry = '${requestScope.employee.firstAidLicenseExpiry}'"
                       />
                <div class="text-right">
                    <span ng-show="errors.firstAidLicenseExpiry" class="ng-hide validation-error">{{errors.firstAidLicenseExpiry}}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">
                Upload First Aid License No <span class="required-field">*</span>
            </label>
            <div class="input-group-sm">
                <input type="file" 
                       id="firstAidFile"
                       name="firstAidFile"
                       class="form-control" 
                       accept="application/pdf,image/*"
                       />
            </div>
            <c:if test="${not empty requestScope.employee.firstAidLicenseFile}">
                <div class="input-group-sm" style="text-align: right; margin-top: 5px">
                    <button class="btn-attachment" type="button" onclick="window.open('${baseUrl}/secured/employee/view/attachment/first-aid/${requestScope.employee.encryptedKey}', '_blank');">
                        View Attachment <span class="glyphicon glyphicon-paperclip"></span>
                    </button>
                </div>
            </c:if>
        </div>
        <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
            <%@page import="rpo.enums.DayEnum"%>

            <%
                request.setAttribute("daysEnum", DayEnum.values());
            %>

            <div class="table-responsive">
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <thead>
                        <tr>
                            <th colspan="3" class="text-center">EMPLOYEE AVAILABILITY</th>
                        </tr>
                        <tr>
                            <th>Day</th>
                            <th class="text-center">Yes/No</th>
                            <th class="text-center">Time (Start-End)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${not empty requestScope.employee.employeeAvailabilities}">
                                <c:forEach varStatus="ctr" var="day" items="${requestScope.employee.employeeAvailabilities}">
                                    <tr>
                                        <td>
                                            <input type="hidden" 
                                                   ng-model="employeeAvailabilities[${ctr.index}].encryptedKey" 
                                                   ng-init="employeeAvailabilities[${ctr.index}].encryptedKey = '${day.encryptedKey}'"/>

                                            <input type="hidden" 
                                                   ng-model="employeeAvailabilities[${ctr.index}].createdBy.username" 
                                                   ng-init="employeeAvailabilities[${ctr.index}].createdBy.username = '${day.createdBy.username}'"/>

                                            <input type="hidden" 
                                                   ng-model="employeeAvailabilities[${ctr.index}].createdDate" 
                                                   ng-init="employeeAvailabilities[${ctr.index}].createdDate = '${day.createdDate}'"/>

                                            <c:if test="${not empty requestScope.employee.encryptedKey}">
                                                <input type="hidden" 
                                                       ng-model="employeeAvailabilities[${ctr.index}].employee.encryptedKey" 
                                                       ng-init="employeeAvailabilities[${ctr.index}].employee.encryptedKey = '${requestScope.employee.encryptedKey}'"/>
                                            </c:if>

                                            <input type="hidden" 
                                                   ng-model="employeeAvailabilities[${ctr.index}].freeDay" 
                                                   ng-init="employeeAvailabilities[${ctr.index}].freeDay = '${day.freeDay}'"/>

                                            ${day.strDay}
                                        </td>
                                        <td class="text-center">
                                            <input type="checkbox" 
                                                   ${day.available ? 'checked="checked"' : ''}
                                                   name="available${day.freeDay}"
                                                   class="available-option"
                                                   />
                                        </td>
                                        <td class="text-center">
                                            <input type="text"
                                                   name="freeTime${day.freeDay}"
                                                   class="text-center timepicker"
                                                   value="${day.freeTime}"
                                                   style="width: 80px" />

                                            <input type="text"
                                                   name="freeTimeEnd${day.freeDay}"
                                                   class="text-center timepicker"
                                                   value="${day.freeTimeEnd}"
                                                   style="width: 80px" />
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach varStatus="ctr" var="day" items="${requestScope.daysEnum}">
                                    <tr>
                                        <td>
                                            ${day.label}
                                            <input type="hidden"
                                                   ng-model="employeeAvailabilities[${ctr.index}].freeDay" 
                                                   ng-init="employeeAvailabilities[${ctr.index}].freeDay = '${day.id}'"/>

                                            <c:if test="${not empty requestScope.employee.encryptedKey}">
                                                <input type="hidden" 
                                                       ng-model="employeeAvailabilities[${ctr.index}].employee.encryptedKey" 
                                                       ng-init="employeeAvailabilities[${ctr.index}].employee.encryptedKey = '${requestScope.employee.encryptedKey}'"/>
                                            </c:if>
                                        </td>
                                        <td class="text-center">
                                            <input type="checkbox"
                                                   name="available${day.id}"
                                                   class="available-option"
                                                   />
                                        </td>
                                        <td class="text-center">
                                            <input type="text"
                                                   name="freeTime${day.id}"
                                                   class="text-center timepicker"
                                                   title="Start Time"
                                                   style="width: 80px" />

                                            <input type="text"
                                                   name="freeTimeEnd${day.id}"
                                                   class="text-center timepicker"
                                                   title="End Time"
                                                   style="width: 80px" />
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>

                    </tbody>
                </table>
            </div>
        </security:authorize>
    </div>
</div>
<div class="row">                    
    <div class="col-md-12 text-left">
        <span class="required-field">Fields marked with * are required.</span>
    </div>
</div>
<hr>