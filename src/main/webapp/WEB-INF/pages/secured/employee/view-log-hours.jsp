<%@include file="../../common/taglibs.jsp" %>

<div ng-controller="viewLogHoursCtrl">
    <input type="hidden" 
           class="form-control " 
           ng-model="employee.encryptedKey"    
           ng-init="employee.encryptedKey = '${requestScope.employee.encryptedKey}'"
           />

    <div class="panel panel-primary" ng-init="initLogs()">
        <div class="panel-heading">
            LOGGED HOURS  

            <div class="btn-group">

                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">                    
                    <span ng-show="filterBy" class="ng-hide">{{filterBy.label}}</span>  <span class="caret" ng-click="filter()"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a ng-click="filter({id: 'all', label: 'All'})">All</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'today', label: 'Today'})">Today</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'this-week', label: 'This Week'})">This Week</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'last-week', label: 'Last Week'})">Last Week</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'this-month', label: 'This Month'})">This Month</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'last-month', label: 'Last Month'})">Last Month</a>
                    </li>
                    <li>
                        <a ng-click="filter({id: 'last-6-months', label: 'Last 6 Months'})">Last 6 Months</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="row" style="">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="alert alert-warning ng-hide" ng-show="!showLoading && (!items.length || items.length === 0)">
                        <p style="margin-bottom: 0px; font-size: 13px">
                            <span class="glyphicon glyphicon-exclamation-sign"></span> No record/s found.
                        </p>
                    </div>

                    <div class="alert alert-success" ng-show="showLoading" style="background-color: white">
                        <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                            Please wait, retrieving records <img src="${baseUrl}/resources/img/loader.gif" />
                        </p>
                    </div>
                    <div class="ng-hide" ng-show="items.length > 0" >

                        <div style="height: 70vh; overflow: auto; padding-right: 2px">
                            <div>
                                <table id="tblLogHours" class="table table-condensed table-hover scroll">                                   
                                    <tbody>
                                        <tr ng-repeat="log in items">

                                            <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                                                <td class="text-center" style="width: 10%; vertical-align: middle">                                                    
                                                    <span ng-show="log.logStatus == 'U'">
                                                        <input type="checkbox" value="{{log.encryptedKey}}"/>                                                
                                                    </span>
                                                </td>
                                            </security:authorize>
                                            <td style="width: 60%; word-wrap: break-word">
                                                <table class="log-hours-table">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center" colspan="4">LOG DETAILS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-stats"></span> Company
                                                            </td>
                                                            <td>{{log.company}}</td>
                                                            <td>
                                                                <span class="glyphicon glyphicon-plane"></span> Location
                                                            </td>
                                                            <td>{{log.location}}</td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-time"></span> Start
                                                            </td>
                                                            <td>{{log.startDatetime}}</td>
                                                            <td>
                                                                <span class="glyphicon glyphicon-time"></span> End
                                                            </td>
                                                            <td>{{log.startDatetime}}</td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-list-alt"></span> Total</td>
                                                            <td colspan="3">{{log.totalHours}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-pencil"></span> Remarks
                                                            </td>
                                                            <td colspan="3">{{log.remarks}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>                                                
                                            </td>
                                            <td style="width: 30%">
                                                <table class="log-hours-table">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center" colspan="4">PAYMENT DETAILS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 30%">
                                                                <span class="glyphicon glyphicon-edit"></span> Status
                                                            </td>
                                                            <td>
                                                                {{log.strLogStatus}}
                                                            </td>
                                                        </tr>
                                                        <tr class="ng-hide" ng-show="{{log.logStatus == 'P'}}">
                                                            <td style="width: 30%">
                                                                <span class="glyphicon glyphicon-saved"></span> Paid On
                                                            </td>
                                                            <td>
                                                                {{log.createdDate}}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                <hr>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-success btn-sm" ng-click="pay()">PAY SELECTED HOURS</button>
                        |
                        <button class="btn btn-danger btn-sm" ng-click="cancel()">CANCEL</button>
                        <security:authorize access="hasRole('ROLE_SUPER_ADMIN')">
                            |
                            <button class="btn btn-danger btn-sm" ng-click="delete()">DELETE</button>
                        </security:authorize>
                    </div>
                </div>   
            </security:authorize>
        </div>    
    </div>

    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <input type="text" name="actionType"/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="paymentModalLabel"></h5>
                </div>
                <div class="modal-body">
                    <p id="promptMessage"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-primary btn-sm" ng-click="confirm()">YES</button>
                </div>
            </div>
        </div>
    </div>
</div>

