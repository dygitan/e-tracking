<%@include file="../../common/taglibs.jsp" %>

<div ng-app="employeeModule">
    <div  class="row" ng-controller="manageCtrl">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">EMPLOYEE INFORMATION</div>
                <div class="panel-body">    
                    <form method="post" enctype="multipart/form-data" ng-submit="save()" novalidate>
                        <%@include file="../../common/employee-info.jsp" %>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success btn-sm" type="submit">SUBMIT</button>
                                |
                                <button class="btn btn-danger btn-sm">
                                    <a href="${baseUrl}/secured/employee/view/all">BACK</a>
                                </button>
                            </div>
                        </div>    
                    </form>
                </div>
            </div>
        </div>
        <c:if test="${not empty requestScope.employee.encryptedKey}">
            <div class="col-xs-12 col-sm-7 col-md-5 col-lg-7">
                <%@include file="view-log-hours.jsp" %>
            </div>
        </c:if>
    </div>
</div>