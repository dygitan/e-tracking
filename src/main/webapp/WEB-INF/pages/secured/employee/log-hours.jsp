<%@include file="../../common/taglibs.jsp" %>

<div class="row" ng-app="employeeModule" ng-controller="logHourCtrl">
    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
        <div class="panel panel-primary" >
            <div class="panel-heading">LOG HOUR</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" 
                               class="form-control " 
                               ng-model="employee.encryptedKey"    
                               ng-init="employee.encryptedKey = '${requestScope.employee.encryptedKey}'"
                               />

                        <div class="form-group">
                            <label for="">
                                Start Date & Time <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text"
                                       id="startDateTime"
                                       class="form-control datetimepicker" 
                                       ng-model="employeeLog.startDatetime"                                       
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.startDatetime" class="ng-hide validation-error">{{errors.startDatetime}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">
                                End Date & Time <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       id="endDateTime"
                                       class="form-control datetimepicker" 
                                       ng-model="employeeLog.endDatetime"   
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.endDatetime" class="ng-hide validation-error">{{errors.endDatetime}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">
                                Total Logged Hours
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"
                                       readonly="true"
                                       id="totalLoggedHours"
                                       />
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="">
                                Company <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"
                                       ng-model="employeeLog.company"   
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.company" class="ng-hide validation-error">{{errors.company}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">
                                Location <span class="required-field">*</span>
                            </label>
                            <div class="input-group-sm">
                                <input type="text" 
                                       class="form-control"
                                       ng-model="employeeLog.location" 
                                       />
                                <div class="text-right">
                                    <span ng-show="errors.location" class="ng-hide validation-error">{{errors.location}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">
                                Remarks
                            </label>
                            <div class="input-group-sm">
                                <textarea  
                                    id="logRemarks"
                                    class="form-control" 
                                    ng-model="employeeLog.remarks">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-md-12 text-left">
                        <span class="required-field">Fields marked with * are required.</span>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-success btn-sm" ng-click="save()">SAVE</button>
                        |
                        <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                            <button class="btn btn-danger btn-sm">
                                <a href="${baseUrl}/secured/employee/view/all">CANCEL</a>
                            </button>
                        </security:authorize>
                        <security:authorize access="hasRole('ROLE_EMPLOYEE')">
                            <button class="btn btn-danger btn-sm">
                                <a href="" ng-click="reset()">RESET</a>
                            </button>
                        </security:authorize>
                    </div>
                </div>    
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
        <%@include file="view-log-hours.jsp" %>
    </div>
</div>