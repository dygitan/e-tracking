<%@include file="../../common/taglibs.jsp" %>

<div class="row" ng-app="employeeModule">
    <div ng-controller="viewEmployeeCtrl">
        <div class="col-lg-2" style="display: none">
            <div class="panel panel-info text-left">
                <div class="panel-heading text-center">
                    <span class="glyphicon glyphicon-filter"></span> <b style="font-size: 13px">FILTER EMPLOYEES</b>
                </div>
                <div class="panel-body text-left">
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" >
                <div class="panel panel-primary">
                    <div class="panel-heading">EMPLOYEES</div>
                    <div class="panel-body">
                        <div class="alert alert-warning ng-hide" ng-show="!showLoading && (!items.length || items.length === 0)">
                            <p style="margin-bottom: 0px; font-size: 13px">
                                <span class="glyphicon glyphicon-exclamation-sign"></span> No record/s found.
                            </p>
                        </div>

                        <div class="alert alert-success" ng-show="showLoading" style="background-color: white">
                            <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                                Please wait, retrieving records <img src="${baseUrl}/resources/img/loader.gif" />
                            </p>
                        </div>
                        <div class="boom" style="height: 75vh; overflow: auto; padding-right: 5px;">
                            <div class="list-group ng-hide" ng-show="items.length > 0">
                                <style>
                                    .float-right {
                                        float: right;
                                    }
                                </style>

                                <a href="${baseUrl}/secured/employee/view/{{employee.encryptedKey}}" class="list-group-item" ng-repeat="employee in items" id="{{employee.encryptedKey}}">
                                    <strong>{{employee.lastName}}, {{employee.firstName}}</strong>
                                    <security:authorize access="hasRole('ROLE_SUPER_ADMIN')">
                                        <span class="glyphicon glyphicon-trash float-right" title="Delete" ng-click="delete($event)"></span>                                    
                                    </security:authorize>
                                </a>

                            </div>
                        </div>
                    </div>    
                </div>    
            </div>

            <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title">CONFIRM DELETION</h5>
                        </div>
                        <div class="modal-body" encrypted-key="{{encryptedKey}}">
                            Are you sure you want to delete <strong>{{employeeName}}</strong>?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">CANCEL</button>
                            <button type="button" class="btn btn-primary btn-sm" ng-click="confirmDeletion()">YES</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
