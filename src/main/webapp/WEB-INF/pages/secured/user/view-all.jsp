<%@include file="../../common/taglibs.jsp" %>

<div ng-app="userModule">
    <div class="row" ng-controller="manageUserCtrl">    
        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-2">
            <div class="panel panel-primary">
                <div class="panel-heading">USER INFORMATION</div>
                <div class="panel-body">  
                    <div class="form-group">
                        <input type="hidden" ng-model="user.password" />

                        <label for="">
                            Username <span class="required-field">*</span>
                        </label>
                        <div class="input-group-sm">
                            <input type="text" 
                                   class="form-control" 
                                   maxlength="45"
                                   ng-model="user.username"
                                   />
                            <div class="text-right">
                                <span ng-show="errors.username" class="ng-hide validation-error">{{errors.username}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            Email <span class="required-field">*</span>
                        </label>
                        <div class="input-group-sm">
                            <input type="text" 
                                   class="form-control" 
                                   maxlength="45"
                                   ng-model="user.email"
                                   />
                            <div class="text-right">
                                <span ng-show="errors.email" class="ng-hide validation-error">{{errors.email}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            Enabled
                        </label>
                        <input type="checkbox"
                               ng-model="user.enabled"
                               ng-init="user.enabled = true"
                               />
                    </div>
                    <div class="form-group">
                        <label for="">
                            Role <span class="required-field">*</span>
                        </label>
                        <div class="input-group-sm">
                            <select 
                                class="form-control"
                                ng-model="user.roleId">
                                <option value="null">--Select--</option>
                                <option value="3">Super Admin</option>                                
                                <option value="1">Admin</option>
                                <option value="2">Employee</option>
                            </select>
                            <div class="text-right">
                                <span ng-show="errors.roleId" class="ng-hide validation-error">{{errors.roleId}}</span>
                                <span ng-show="errors.notEmployeeUser" class="ng-hide validation-error">{{errors.notEmployeeUser}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            Password <span class="required-field">*</span>
                        </label>
                        <div class="input-group-sm">
                            <input type="password" 
                                   class="form-control" 
                                   maxlength="45"
                                   ng-model="user.enteredPassword"
                                   />
                            <div class="text-right">
                                <span ng-show="errors.enteredPassword" class="ng-hide validation-error">{{errors.enteredPassword}}</span>
                                <span ng-show="errors.passwordValid" class="ng-hide validation-error">{{errors.passwordValid}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">
                            Confirm Password <span class="required-field">*</span>
                        </label>
                        <div class="input-group-sm">
                            <input type="password" 
                                   class="form-control" 
                                   maxlength="45"
                                   ng-model="user.confirmPassword"
                                   />
                            <div class="text-right">
                                <span ng-show="errors.confirmPassword" class="ng-hide validation-error">{{errors.confirmPassword}}</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="text-right">
                        <button class="btn btn-success btn-sm" type="button" ng-click="submit()">SUBMIT</button>
                        |
                        <button class="btn btn-danger btn-sm" ng-click="clear()">CLEAR</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">USERS</div>
                <div class="panel-body">
                    <div>


                        <style>
                            table {
                                /*table-layout: fixed*/
                            }

                            table a:hover {
                                cursor: pointer;
                                font-weight: bold;
                                text-decoration: none;
                            }
                        </style>
                        <table class="table table-condensed table-bordered table-hover" style="margin-bottom: 0px">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 15%">Username</th>
                                    <th class="text-center" style="width: 30%">Email</th>
                                    <th class="text-center" style="width: 20%">Role</th>
                                    <th class="text-center">Enabled</th>
                                    <th style="width: 10%">&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        <div style="height: 75vh; overflow: auto; border: 1px solid #ddd; width: 99.9%">
                            <table class="table table-condensed table-bordered table-hover">
                                <tbody>
                                    <tr ng-hide="!showLoading">
                                        <td colspan="4">
                                            <div class="alert alert-warning ng-hide" ng-show="!showLoading && (!items.length || items.length === 0)">
                                                <p style="margin-bottom: 0px; font-size: 13px">
                                                    <span class="glyphicon glyphicon-exclamation-sign"></span> No record/s found.
                                                </p>
                                            </div>

                                            <div class="alert alert-success" ng-show="showLoading" style="background-color: white">
                                                <p style="margin-bottom: 0px; font-size: 13px" class="text-success">
                                                    Please wait, retrieving records <img src="${baseUrl}/resources/img/loader.gif" />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="ng-hide" ng-repeat="user in items" ng-show="items.length > 0">
                                        <td style="width: 15%">
                                            {{user.username}}
                                        </td>
                                        <td style="width: 30%">
                                            {{user.email}}
                                        </td>
                                        <td class="text-center" style="width: 20%">
                                            {{user.role}}
                                        </td>
                                        <td class="text-center">
                                            {{user.enabled ? 'Yes' : 'No'}}
                                        </td>
                                        <td class="text-center" style="width: 10%">
                                            <a ng-click="edit(user)">Edit</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>  
            </div>  
        </div>
    </div>
</div>