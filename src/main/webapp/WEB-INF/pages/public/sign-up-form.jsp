<%@include file="../common/taglibs.jsp" %>

<div>
    <div class="row" ng-controller="signupCtrl">
        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
                        EMPLOYEE INFORMATION
                    </security:authorize>
                    <security:authorize access="hasRole('ROLE_EMPLOYEE')">
                        MY PROFILE
                    </security:authorize>
                    <security:authorize access="isAnonymous()">
                        SIGN UP
                    </security:authorize>
                </div>
                <div class="panel-body">    
                    <%@include file="../common/employee-info.jsp" %>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success btn-sm" ng-click="save()">SAVE</button>
                            |
                            <button class="btn btn-danger btn-sm" ng-click="clear()">CLEAR</button>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
