package rpo.aop;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import rpo.config.spring.SpringConfig;

/**
 *
 * @author Patrick Tan
 * @since Dec 7, 2014
 */
@ContextConfiguration(classes = {SpringConfig.class})
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@EnableAspectJAutoProxy
//@Transactional
public class LogginAspectIT {

//    @Autowired
//    JavaMailSender mailSender;
//    private ClientService ClientService;
    @Test
    public void hello() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setUsername("incognitos.freelance");
        mailSender.setPassword("globe@123");

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        mailSender.setJavaMailProperties(props);

        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
            helper.setTo("incognitos.freelance@gmail.com");
            helper.setText("<strong>Hello World</strong> <br> hello world again", true);

        } catch (MessagingException ex) {
            ex.printStackTrace(System.err);
        }

        mailSender.send(message);
    }
}
