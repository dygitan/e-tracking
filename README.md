A web based application used for employees timesheet logging/recording.

Technologies used for this project are:

- Java
- Spring
- Hibernate
- Apache Tiles
- MySQL
- Maven
- Bootstrap
- JQuery

Demo application can be found: http://portfolio-etrack.herokuapp.com

Login credentials:

- admin:123456 (admin)
- mickey:123456 (employee)
